<!-- product -->
<div class="product">
    <div class="product-img">
        <div class="picture">
            @if (Route::is('shop') && $product->quantity == 0)
                <div class="stamp red">تمام شد</div>
            @endif
            @php
                $unserialize_images = unserialize($product->image);
            @endphp
            <img src="/{{$unserialize_images[0]}}" alt="" width="540" height="374" />
            <div class="img-overlay">
                <a href="{{ route('product', $product->code) }}" class="btn more btn-primary">توضیحات بیشتر</a>
            </div>
        </div>
    </div>
    <div class="main-titles no-margin">
        <h4 class="title">{{$product->title}}</h4>
        </br>
        <h5 class="no-margin">{{number_format($product->price)}} تومان</h5>
    </div>
    <p class="desc">{{$product->description}}</p>
    <p class="center-align stars">
        <span class="icon-star stars-clr"></span>
        <span class="icon-star stars-clr"></span>
        <span class="icon-star"></span>
        <span class="icon-star"></span>
        <span class="icon-star"></span>
    </p>
</div>
