<!--  Main Menu / navbar  -->
<div class="navbar navbar-static-top" id="stickyNavbar">
    <div class="navbar-inner">
        <div class="container">
            <div class="row">
                <div class="span{{!isset($user) || Route::is('cart_step1', 'cart_step2', 'cart_step3', 'cart_step4') ? 12 : 9}}">
                    <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>

                    <!--  ========  -->
                    <!--  = Menu =  -->
                    <!--  ========  -->
                    <div class="nav-collapse collapse">
                        <ul class="nav" id="mainNavigation">
                            <li class="dropdown {{ Route::is('index') ? 'active' : null }}">
                                <a href="{{ route('index') }}" class="dropdown-toggle"> خانه</a>
                            </li>

                            <li class="dropdown {{ Route::is('shop') ? 'active' : null }}">
                                <a href="{{ route('shop') }}" class="dropdown-toggle"> فروشگاه &nbsp; <b class="caret"></b> </a>
                                <ul class="dropdown-menu">
                                </ul>
                            </li>
                            <li class="{{ Route::is('about-us') ? 'active' : null }}"><a href="{{ route('about-us') }}">درباره ما</a></li>
                            <li class="{{ Route::is('contact-us') ? 'active' : null }}"><a href="{{ route('contact-us') }}">تماس با ما</a></li>
                        </ul>


                        <!--  ===============  -->
                        <!--  = Search form =  -->
                        <!--  ===============  -->
                        <form class="navbar-form pull-right" action="#" method="get">
                            <button type="submit"><span class="icon-search "></span></button>
                            <input type="text" class="span1" name="search" id="navSearchInput">
                        </form>
                    </div>
                    <!-- /.nav-collapse -->
                </div>

                <!--  ========  -->
                <!--  = Cart =  -->
                <!--  ========  -->

                @if (isset($user) && !Route::is('cart_step1', 'cart_step2', 'cart_step3', 'cart_step4'))
                    @include('partials.cart')
                @endif

                <!-- /cart -->

            </div>
        </div>
    </div>
</div>