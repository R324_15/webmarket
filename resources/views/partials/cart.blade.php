<!-- cart -->
<div class="span3">
    <div class="cart-container" id="{{count($cart_products) == 0 ? NULL : 'cartContainer'}}">
        <div class="cart">
            <p class="items">سبد خرید <span class="dark-clr">({{count($cart_products)}})</span></p>
            @php
                $sum_price = 0;
                $i = 0;
                foreach ($cart_products as $product) {
                    $price = $product->price * $carts[$i]->product_quantity;
                    $sum_price += $price;
                    $i++;
                }
            @endphp
            <p class="dark-clr hidden-tablet">{{count($cart_products) > 0 ? number_format($sum_price += 15000) : NULL}} تومان</p>
            <a href="{{ route('cart_step1') }}" class="btn btn-danger {{count($cart_products) == 0 ? 'disabled' : NULL}}" onclick="{{count($cart_products) == 0 ? 'return false' : NULL}}">
                <i class="icon-shopping-cart"></i>
            </a>
        </div>
        <div class="open-panel">
            @php
                $i = 0;
                foreach ($cart_products as $product) {
                    $unserialize_images = unserialize($product->image);
                        if ($i == 5) {
                            echo '<div class="item-in-cart clearfix">';
                                echo '<div class="desc">';
                                    echo '<a href="' . route('cart_step1') . '">ادامه محصولات</a>';
                                echo '</div>';
                            echo '</div>';
                            break;
                        } else {
                    echo '<div class="item-in-cart clearfix">';
                        echo '<div class="image">';
                        echo '<a href="' . route('product', $product->code) . '">';
                            echo '<img src="/' . $unserialize_images[0] . '" width="124" height="124" alt="cart item" />';
                        echo '</a>';
                        echo '</div>';
                        echo '<div class="desc">';
                            echo '<strong><a href="' . route('product', $product->code) . '">' . $product->title . '</a></strong>';
                            echo '<span class="light-clr qty">
                                    تعداد : ';
                                        if ($product->code == $carts[$i]->product->code) {
                                            echo $carts[$i]->product_quantity;
                                    echo '&nbsp;
                                    <a href="' . route('cart_delete', $carts[$i]->id) . '">حذف</a>
                                </span>';
                        echo '</div>';
                        echo '<div class="price">';
                            echo '<strong>';
                                            echo number_format($price = $product->price * $carts[$i]->product_quantity);
                                        }
                                echo '</br>تومان';
                            echo '</strong>';
                        echo '</div>';
                    echo '</div>';
                        }
                    $i++;
                }
            @endphp
            <div class="summary">
                <div class="line">
                    <div class="row-fluid">
                        <div class="span6 align-right">15,000 تومان</div>
                        <div class="span6">هزینه ارسال :</div>
                    </div>
                </div>
                <div class="line">
                    <div class="row-fluid">
                        <div class="span6 align-right size-16">{{count($cart_products) > 0 ? number_format($sum_price) : NULL}} تومان</div>
                        <div class="span6">جمع کل :</div>
                    </div>
                </div>
            </div>
            <div class="proceed">
                <a href="{{ route('cart_step1') }}" class="btn btn-danger pull-right bold higher">تصویه حساب <i class="icon-shopping-cart"></i></a>
                <small>هزینه ارسال بر اساس منطقه جغرافیایی محاسبه میشود. <!-- <a href="#">اطلاعات بیشتر</a> --></small>
            </div>
        </div>
    </div>
</div>
