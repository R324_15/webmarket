<!--  Header  -->
<header id="header">
    <div class="container">
        <div class="row">

            <!--  ========  -->
            <!--  = Logo =  -->
            <!--  ========  -->
            <div class="span3">
                <a class="brand" href="{{ route('index') }}">
                    <img src="{{ asset('images/logo.png') }}" alt="Webmarket Logo" width="48" height="48" /> 
                    <span class="pacifico">Webmarket</span> 
                </a>
            </div>
            <div class="span4"></div>

            <!--  ================  -->
            <!--  = Social Icons =  -->
            <!--  ================  -->
            <div class="span5">
                <div class="topmost-line"></div>
                <div class="top-right">
                    <div class="icons">
                        <a href="http://www.facebook.com/ProteusNet"><span class="zocial-facebook"></span></a>
                        <a href="skype:primozcigler?call"><span class="zocial-skype"></span></a>
                        <a href="https://twitter.com/proteusnetcom"><span class="zocial-twitter"></span></a>
                        <a href="http://eepurl.com/xFPYD"><span class="zocial-rss"></span></a>
                    </div>
                    <div class="register">
                        <a class="btn btn-danger input-block-level bold higher" href="{{ Auth::check() ? route('admin') : '#loginModal' }}" role="button" data-toggle="modal"><span class="icon-user"></span>&nbsp;&nbsp;{{ Auth::check() ? 'پنل کاربری' : 'ورود / ثبت نام' }}</a>
                    </div>
                </div>
            </div>
            <!-- /social icons -->
        </div>
    </div>
</header>
