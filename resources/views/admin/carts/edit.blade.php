@extends('admin.layouts.admin')

@section('title') ویرایش سبد خرید @endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    @include('admin.partials.message')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            ویرایش
            <small>سبد خرید</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin') }}"><i class="fa fa-dashboard"></i> داشبورد</a></li>
            <li><a href="{{ route('carts') }}">سبد خرید</a></li>
            <li class="active"><a href="{{ route('carts_edit', $cart->id) }}">ویرایش</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form class="form-horizontal" action="{{ route('carts_update', $cart->id) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">
                        <div class="col-sm-1"></div>
                        <label for="User" class="col-sm-2 col-form-label">کاربر</label>
                        <select class="col-sm-8 form-control" size="1" name="User" id="User">
                            @foreach ($users as $user)
                                @if ($user->id == $cart->user_id)
                                    <option value="{{$cart->user_id}}" selected>{{$user->name}}</option>
                                @else
                                    <option value="{{$user->id}}">{{$user->name}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-1"></div>
                        <label for="Product" class="col-sm-2 col-form-label">محصول</label>
                        <select class="col-sm-8 form-control" size="1" name="Product" id="Product">
                            @foreach ($products as $product)
                                @if ($product->id == $cart->product_id)
                                    <option value="{{$cart->product_id}}" selected>{{$product->title}}</option>
                                @else
                                    <option value="{{$product->id}}">{{$product->title}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-1"></div>
                        <label for="Color" class="col-sm-2 col-form-label">رنگ</label>
                        <select class="col-sm-8 form-control" size="1" name="Color" id="Color">
                            @foreach ($cart->product->product_colors as $product_color)
                                @if ($product_color->name == $cart->product_color)
                                    <option value="{{$cart->product_color}}" selected>{{$cart->product->title}} => {{$product_color->name}}</option>
                                @else
                                    <option value="{{$product_color->name}}">{{$cart->product->title}} => {{$product_color->name}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-1"></div>
                        <label for="Size" class="col-sm-2 col-form-label">سایز</label>
                        <select class="col-sm-8 form-control" size="1" name="Size" id="Size">
                            @foreach ($cart->product->product_sizes as $product_size)
                                @if ($product_size->title == $cart->product_size)
                                    <option value="{{$cart->product_size}}" selected>{{$cart->product->title}} => {{$product_size->title}}</option>
                                @else
                                    <option value="{{$product_size->title}}">{{$cart->product->title}} => {{$product_size->title}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-1"></div>
                        <label for="Quantity" class="col-sm-2 col-form-label">تعداد</label>
                        <input type="text" class="col-sm-8 form-control" value="{{$cart->product_quantity}}" name="Quantity" id="Quantity">
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-5"></div>
                        <button type="submit" class="col-sm-2 btn btn-success" name="btn">ثبت</button>
                    </div>
                </form>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection
