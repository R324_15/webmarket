@extends('admin.layouts.admin')

@section('title') افزودن سبد خرید @endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
    <h1>
        افزودن
        <small>سبد خرید</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin') }}"><i class="fa fa-dashboard"></i> داشبورد</a></li>
        <li><a href="{{ route('carts') }}">سبد خرید</a></li>
        <li class="active"><a href="{{ route('carts_create') }}">افزودن</a></li>
    </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form action="{{ route('carts_store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">
                        <div class="col-sm-1"></div>
                        <label for="User" class="col-sm-2 col-form-label">کاربر</label>
                        <select class="col-sm-6 form-control" size="1" name="User" id="User">
                            @foreach ($users as $user)
                                <option value="{{$user->id}}">{{$user->name}}</option>
                            @endforeach
                        </select>
                        <div class="col-sm-1"></div>
                        <a class="col-sm-1 btn btn-primary" href="{{ route('users_create') }}">افزودن</a>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-1"></div>
                        <label for="Product" class="col-sm-2 col-form-label">محصول</label>
                        <select class="col-sm-6 form-control" size="1" name="Product" id="Product">
                            @foreach ($products as $product)
                                <option value="{{$product->id}}">{{$product->title}}</option>
                            @endforeach
                        </select>
                        <div class="col-sm-1"></div>
                        <a class="col-sm-1 btn btn-primary" href="{{ route('products_create') }}">افزودن</a>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-1"></div>
                        <label for="Color" class="col-sm-2 col-form-label">رنگ</label>
                        <select class="col-sm-6 form-control" size="1" name="Color" id="Color">
                            @foreach ($products as $product)
                                @foreach ($product->product_colors as $product_color)
                                    <option value="{{$product_color->name}}">{{$product->title}} => {{$product_color->name}}</option>
                                @endforeach
                            @endforeach
                        </select>
                        <div class="col-sm-1"></div>
                        <a class="col-sm-1 btn btn-primary" href="{{ route('colors_create') }}">افزودن</a>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-1"></div>
                        <label for="Size" class="col-sm-2 col-form-label">سایز</label>
                        <select class="col-sm-6 form-control" size="1" name="Size" id="Size">
                            @foreach ($products as $product)
                                @foreach ($product->product_sizes as $product_size)
                                    <option value="{{$product_size->title}}">{{$product->title}} => {{$product_size->title}}</option>
                                @endforeach
                            @endforeach
                        </select>
                        <div class="col-sm-1"></div>
                        <a class="col-sm-1 btn btn-primary" href="{{ route('sizes_create') }}">افزودن</a>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-1"></div>
                        <label for="Quantity" class="col-sm-2 col-form-label">تعداد</label>
                        <input type="number" class="col-sm-8 form-control" name="Quantity" id="Quantity">
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-5"></div>
                        <button type="submit" class="col-sm-2 btn btn-success" name="btn"><b>ثبت</b></button>
                    </div>
                </form>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection
