@extends('admin.layouts.admin')

@section('title') سبد خرید @endsection

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        @include('admin.partials.message')

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                لیست
                <small>سبد خرید</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('admin') }}"><i class="fa fa-dashboard"></i> داشبورد</a></li>
                <li class="active"><a href="{{ route('carts') }}">سبد خرید</a></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <a href="{{ route('carts_create') }}">
                                <button type="button" class="btn btn-block btn-success btn-flat">افزودن سبد خرید جدید</button>
                            </a>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>شماره</th>
                                        <th>کاربر</th>
                                        <th>محصول</th>
                                        <th>رنگ</th>
                                        <th>سایز</th>
                                        <th>تعداد</th>
                                        <th>ویرایش</th>
                                        <th>حذف</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($carts as $cart)
                                        <tr>
                                            <td>{{$cart->id}}</td>
                                            <td>{{$cart->user->name}}</td>
                                            <td><a href="{{ route('product', $cart->product->code) }}">{{$cart->product->title}}</a></td>
                                            <td>{{$cart->product_color}}</td>
                                            <td>{{$cart->product_size}}</td>
                                            <td>{{$cart->product_quantity}}</td>
                                            <td><a href="{{ route('carts_edit', $cart->id) }}" class="btn btn-primary fa fa-edit"></a></td>
                                            <td><a href="{{ route('carts_delete', $cart->id) }}" class="btn btn-danger fa fa-remove"></a></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@section('scripts')

<!-- page script -->
<script>
    $(function () {
        $('#example1').DataTable()
    })
</script>

@endsection

@endsection
