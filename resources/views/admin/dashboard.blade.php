@extends('admin.layouts.admin')

@section('title') داشبورد @endsection

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        داشبورد
        <small>کنترل پنل</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('index') }}"><i class="fa fa-home"></i> خانه</a></li>
        <li class="active"><a href="{{ route('admin') }}"><i class="fa fa-dashboard"></i> داشبورد</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @if (auth()->user()->user_type == 0)
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-xs-6">

            <!-- small box -->
            <div class="small-box bg-aqua">
              <div class="inner">
                <h3>{{count($carts)}}</h3>
                <p>سفارش جدید</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="{{ route('carts') }}" class="small-box-footer">اطلاعات بیشتر <i class="fa fa-arrow-circle-left"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">

            <!-- small box -->
            <div class="small-box bg-green">
              <div class="inner">
                <h3>53<sup style="font-size: 20px">%</sup></h3>
                <p>افزایش امتیاز</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="#" class="small-box-footer">اطلاعات بیشتر <i class="fa fa-arrow-circle-left"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">

            <!-- small box -->
            <div class="small-box bg-yellow">
              <div class="inner">
                <h3>{{count($users)}}</h3>
                <p>کاربران ثبت شده</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="{{ route('users') }}" class="small-box-footer">اطلاعات بیشتر <i class="fa fa-arrow-circle-left"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">

            <!-- small box -->
            <div class="small-box bg-red">
              <div class="inner">
                <h3>65</h3>
                <p>بازدید جدید</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <a href="#" class="small-box-footer">اطلاعات بیشتر <i class="fa fa-arrow-circle-left"></i></a>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->
        @endif

      <!-- Profile row -->
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-body box-profile">
              @if (auth()->user()->user_type == 0)
                <a href="{{ route('users_edit', auth()->user()->id) }}" class="btn btn-primary pull-left">ویرایش</a>
              @elseif (auth()->user()->user_type == 1)
                <a href="{{ route('users_customer_edit', auth()->user()->id) }}" class="btn btn-primary pull-left">ویرایش</a>
              @endif
              <h3 class="profile-username">{{auth()->user()->name}}</h3>
              <p class="text-muted">{{auth()->user()->user_type == 0 ? 'مدیر' : 'مشتری'}}</p>
              <hr>
              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <i class="fa fa-barcode margin-r-5"></i> <b>کد ملی</b>
                  <a class="pull-left">{{auth()->user()->national_code ? auth()->user()->national_code : 'تکمیل نشده است'}}</a>
                </li>
                <li class="list-group-item">
                  <i class="fa fa-paper-plane margin-r-5"></i> <b>ایمیل</b>
                  <a class="pull-left">{{auth()->user()->email ? auth()->user()->email : 'تکمیل نشده است'}}</a>
                </li>
                <li class="list-group-item">
                  <i class="fa fa-map-marker margin-r-5"></i> <b>موقعیت</b>
                  <a class="pull-left">
                    @if(!empty(auth()->user()->province_id) && !empty(auth()->user()->city_id))
                      {{auth()->user()->province->name}}، {{auth()->user()->city->name}}
                    @else
                      تکمیل نشده است
                    @endif
                  </a>
                </li>
                <li class="list-group-item">
                  <i class="fa fa-paper-plane-o margin-r-5"></i> <b>آدرس</b>
                  <a class="pull-left">{{auth()->user()->address ? auth()->user()->address : 'تکمیل نشده است'}}</a>
                </li>
                <li class="list-group-item">
                  <i class="fa fa-file-text-o margin-r-5"></i> <b>کد پستی</b>
                  <a class="pull-left">{{auth()->user()->post_code ? auth()->user()->post_code : 'تکمیل نشده است'}}</a>
                </li>
                <li class="list-group-item">
                  <i class="fa fa-phone margin-r-5"></i> <b>شماره تلفن همراه</b>
                  <a class="pull-left">{{auth()->user()->phone ? auth()->user()->phone : 'تکمیل نشده است'}}</a>
                </li>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      @if (auth()->user()->user_type == 0)
        <!-- Main row -->
        <div class="row">

          <!-- right col -->
          <section class="col-lg-6 connectedSortable">

            <!-- Custom tabs (Charts with tabs)-->
            <div class="nav-tabs-custom">

              <!-- Tabs within a box -->
              <ul class="nav nav-tabs pull-left">
                <li class="active"><a href="#revenue-chart" data-toggle="tab">نمودار</a></li>
                <li><a href="#sales-chart" data-toggle="tab">چارت</a></li>
                <li class="pull-right header"><i class="fa fa-inbox"></i> فروش</li>
              </ul>
              <div class="tab-content no-padding">

                <!-- Morris chart - Sales -->
                <div class="chart tab-pane active" id="revenue-chart" style="position: relative; height: 300px;"></div>
                <div class="chart tab-pane" id="sales-chart" style="position: relative; height: 300px;"></div>
              </div>
            </div>
            <!-- /.nav-tabs-custom -->

          </section>
          <!-- /.right col -->

          <!-- left col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-6 connectedSortable">

            <!-- TO DO List -->
            <div class="box box-primary">
              <div class="box-header">
                <i class="ion ion-clipboard"></i>
                <h3 class="box-title">لیست کارها</h3>
                <div class="box-tools pull-left">
                  <ul class="pagination pagination-sm inline">
                    <li><a href="#">&laquo;</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">&raquo;</a></li>
                  </ul>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">

                <!-- See dist/js/pages/dashboard.js to activate the todoList plugin -->
                <ul class="todo-list">
                  <li>

                    <!-- drag handle -->
                    <span class="handle">
                      <i class="fa fa-ellipsis-v"></i>
                      <i class="fa fa-ellipsis-v"></i>
                    </span>

                    <!-- checkbox -->
                    <input type="checkbox" value="">

                    <!-- todo text -->
                    <span class="text">ساخت قالب</span>

                    <!-- Emphasis label -->
                    <small class="label label-danger"><i class="fa fa-clock-o"></i> ۲ دقیقه</small>

                    <!-- General tools such as edit or delete-->
                    <div class="tools">
                      <i class="fa fa-edit"></i>
                      <i class="fa fa-trash-o"></i>
                    </div>
                  </li>
                  <li>
                    <span class="handle">
                      <i class="fa fa-ellipsis-v"></i>
                      <i class="fa fa-ellipsis-v"></i>
                    </span>
                    <input type="checkbox" value="">
                    <span class="text">بهینه سازی قالب سایت</span>
                    <small class="label label-info"><i class="fa fa-clock-o"></i> ۴ ساعت</small>
                    <div class="tools">
                      <i class="fa fa-edit"></i>
                      <i class="fa fa-trash-o"></i>
                    </div>
                  </li>
                  <li>
                    <span class="handle">
                      <i class="fa fa-ellipsis-v"></i>
                      <i class="fa fa-ellipsis-v"></i>
                    </span>
                    <input type="checkbox" value="">
                    <span class="text">ایجاد صفحه فرود سایت</span>
                    <small class="label label-warning"><i class="fa fa-clock-o"></i> ۱ روز</small>
                    <div class="tools">
                      <i class="fa fa-edit"></i>
                      <i class="fa fa-trash-o"></i>
                    </div>
                  </li>
                  <li>
                    <span class="handle">
                      <i class="fa fa-ellipsis-v"></i>
                      <i class="fa fa-ellipsis-v"></i>
                    </span>
                    <input type="checkbox" value="">
                    <span class="text">تبلیغات سایت</span>
                    <small class="label label-success"><i class="fa fa-clock-o"></i> ۳ روز</small>
                    <div class="tools">
                      <i class="fa fa-edit"></i>
                      <i class="fa fa-trash-o"></i>
                    </div>
                  </li>
                  <li>
                    <span class="handle">
                      <i class="fa fa-ellipsis-v"></i>
                      <i class="fa fa-ellipsis-v"></i>
                    </span>
                    <input type="checkbox" value="">
                    <span class="text">بررسی اعلان ها</span>
                    <small class="label label-primary"><i class="fa fa-clock-o"></i> ۱ هفته</small>
                    <div class="tools">
                      <i class="fa fa-edit"></i>
                      <i class="fa fa-trash-o"></i>
                    </div>
                  </li>
                  <li>
                    <span class="handle">
                      <i class="fa fa-ellipsis-v"></i>
                      <i class="fa fa-ellipsis-v"></i>
                    </span>
                    <input type="checkbox" value="">
                    <span class="text">طراحی سیستم جدید</span>
                    <small class="label label-default"><i class="fa fa-clock-o"></i> ۲ ماه</small>
                    <div class="tools">
                      <i class="fa fa-edit"></i>
                      <i class="fa fa-trash-o"></i>
                    </div>
                  </li>
                </ul>
              </div>
              <!-- /.box-body -->
              <div class="box-footer clearfix no-border">
                <button type="button" class="btn btn-default pull-left"><i class="fa fa-plus"></i> جدید</button>
              </div>
            </div>
            <!-- /.box -->
          </section>
          <!-- left col -->
        </div>
        <!-- /.row (main row) -->
      @endif
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@section('scripts')

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>

@endsection

@endsection
