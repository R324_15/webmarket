<!-- right side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="جستجو">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            @if (auth()->user()->user_type == 0 || auth()->user()->user_type == 1)
                <li class="header">منو</li>

                <!-- Dashboard -->
                <li class="{{ Route::is('admin') ? 'active' : null }}">
                    <a href="{{ route('admin') }}">
                        <i class="fa fa-dashboard"></i> <span>داشبورد</span>
                    </a>
                </li>
            @endif

            @if (auth()->user()->user_type == 0)
                <!-- Users -->
                <li class="{{ Route::is('users', 'users_create', 'users_edit') ? 'active' : null }}">
                    <a href="{{ route('users') }}">
                        <i class="fa fa-users"></i> <span>کاربران</span>
                    </a>
                </li>

                <!-- Products -->
                <li class="{{ Route::is('products', 'products_create', 'products_edit', 'colors', 'colors_create', 'product_colors_create', 'colors_edit', 'types', 'types_create', 'types_edit', 'categories', 'categories_create', 'categories_edit', 'sizes', 'sizes_create', 'product_sizes_create', 'sizes_edit', 'brands', 'brands_create', 'product_brands_create', 'brands_edit') ? 'active' : null }} treeview">
                    <a href="#">
                        <i class="fa fa-shopping-cart"></i> <span>محصولات</span>
                        <span class="pull-left-container">
                        <i class="fa fa-angle-right pull-left"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="{{ Route::is('products') ? 'active' : null }}"><a href="{{ route('products') }}"><i class="fa fa-list-alt"></i>لیست محصولات</a></li>
                        <li class="{{ Route::is('colors') ? 'active' : null }}"><a href="{{ route('colors') }}"><i class="fa fa-paint-brush"></i>رنگ محصولات</a></li>
                        <li class="{{ Route::is('types') ? 'active' : null }}"><a href="{{ route('types') }}"><i class="fa fa-tasks"></i>نوع محصولات</a></li>
                        <li class="{{ Route::is('categories') ? 'active' : null }}"><a href="{{ route('categories') }}"><i class="fa fa-navicon"></i>دسته بندی محصولات</a></li>
                        <li class="{{ Route::is('sizes') ? 'active' : null }}"><a href="{{ route('sizes') }}"><i class="fa fa-navicon"></i>سایز محصولات</a></li>
                        <li class="{{ Route::is('brands') ? 'active' : null }}"><a href="{{ route('brands') }}"><i class="fa fa-navicon"></i>برند محصولات</a></li>
                    </ul>
                </li>

                <!-- Cart -->
                <li class="{{ Route::is('carts', 'carts_create', 'carts_edit') ? 'active' : null }}">
                    <a href="{{ route('carts') }}">
                        <i class="fa fa-opencart"></i> <span>سبد خرید</span>
                    </a>
                </li>
            @endif
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
