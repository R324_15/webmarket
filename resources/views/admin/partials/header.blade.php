<!--  Header  -->
<header class="main-header">

    <!-- Logo -->
    <a href="{{ route('index') }}" class="logo">

        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">پنل</span>

        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>WebMarket</b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">

        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-power-off"></i><span class="hidden-xs">{{auth()->user()->name}}</span>
                    </a>
                    <ul class="dropdown-menu">

                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <p>
                                <b class="text-center">{{auth()->user()->name}}</b>
                                </br>
                                <small>{{auth()->user()->user_type == 0 ? 'مدیر' : 'مشتری'}}</small>
                            </p>
                            <div class="pull-right">
                                @if (auth()->user()->user_type == 0)
                                    <a href="{{ Auth::check() ? route('users_edit', auth()->user()->id) : NULL }}" class="btn btn-primary btn-flat">ویرایش</a>
                                @elseif (auth()->user()->user_type == 1)
                                    <a href="{{ Auth::check() ? route('users_customer_edit', auth()->user()->id) : NULL }}" class="btn btn-primary pull-left">ویرایش</a>
                                @endif
                            </div>
                            <div class="pull-left">
                                <a href="{{ Auth::check() ? route('logout') : NULL }}" class="btn btn-danger btn-flat">خروج</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
