@extends('admin.layouts.admin')

@section('title') کاربران @endsection

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    @include('admin.partials.message')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        لیست
        <small>کاربران</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('admin') }}"><i class="fa fa-dashboard"></i> داشبورد</a></li>
        <li class="active"><a href="{{ route('users') }}">کاربران</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <a href="{{ route('users_create') }}">
                <button type="button" class="btn btn-block btn-success btn-flat">افزودن کاربر جدید</button>
              </a>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>شماره</th>
                    <th>نوع كاربری</th>
                    <th>كد ملی</th>
                    <th>نام</th>
                    <th>ایمیل</th>
                    <th>استان</th>
                    <th>شهر</th>
                    <th>آدرس</th>
                    <th>کد پستی</th>
                    <th>شماره تلفن</th>
                    <th>ویرایش</th>
                    <th>حذف</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($users as $user)
                    <tr>
                      <td>{{$user->id}}</td>
                      <td>
                        @if ($user->user_type == 0)
                          مدیر
                        @elseif ($user->user_type == 1)
                          مشتری
                        @endif
                      </td>
                      <td>{{$user->national_code}}</td>
                      <td>{{$user->name}}</td>
                      <td>{{$user->email}}</td>
                      <td>@if(!empty($user->province_id)) {{$user->province->name}} @endif</td>
                      <td>@if(!empty($user->city_id)) {{$user->city->name}} @endif</td>
                      <td>{{$user->address}}</td>
                      <td>{{$user->post_code}}</td>
                      <td>{{$user->phone}}</td>
                      <td><a href="{{ route('users_edit', $user->id) }}" class="btn btn-primary fa fa-edit"></a></td>
                      <td><a href="{{ route('users_delete', $user->id) }}" class="btn btn-danger fa fa-remove"></a></td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
</div>
<!-- ./wrapper -->

@section('scripts')

<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
  })
</script>

@endsection

@endsection
