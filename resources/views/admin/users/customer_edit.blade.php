@extends('admin.layouts.admin')

@section('title') ویرایش کاربر @endsection

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    @include('admin.partials.message')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        ویرایش
        <small>کاربر</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('admin') }}"><i class="fa fa-dashboard"></i> داشبورد</a></li>
        <li class="active"><a href="{{ route('users_customer_edit', $user->id) }}">ویرایش</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

          <!-- Default box -->
          <div class="box">
        <div class="box-body">
            <form class="form-horizontal" action="{{ route('users_customer_update', $user->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label for="Nationalcode" class="col-sm-2 col-form-label">کد ملی</label>
                    <input type="number" class="col-sm-8 form-control" value="{{$user->national_code}}" name="Nationalcode" id="Nationalcode">
                </div>
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label for="Name" class="col-sm-2 col-form-label">نام</label>
                    <input type="text" class="col-sm-8 form-control" value="{{$user->name}}" name="Name" id="Name">
                </div>
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label for="Email" class="col-sm-2 col-form-label">ایمیل</label>
                    <input type="text" class="col-sm-8 form-control" value="{{$user->email}}" name="Email" id="Email">
                </div>
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label for="Province" class="col-sm-2 col-form-label">استان</label>
                    <input type="text" class="col-sm-8 form-control" value="{{$user->province}}" name="Province" id="Province">
                </div>
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label for="City" class="col-sm-2 col-form-label">شهر</label>
                    <input type="text" class="col-sm-8 form-control" value="{{$user->city}}" name="City" id="City">
                </div>
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label for="Address" class="col-sm-2 col-form-label">آدرس</label>
                    <input type="text" class="col-sm-8 form-control" value="{{$user->address}}" name="Address" id="Address">
                </div>
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label for="Postcode" class="col-sm-2 col-form-label">کد پستی</label>
                    <input type="number" class="col-sm-8 form-control" value="{{$user->post_code}}" name="Postcode" id="Postcode">
                </div>
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label for="Phone" class="col-sm-2 col-form-label">شماره تلفن</label>
                    <input type="number" class="col-sm-8 form-control" value="{{$user->phone}}" name="Phone" id="Phone">
                </div>
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label for="Password" class="col-sm-2 col-form-label">رمز عبور</label>
                    <input type="text" class="col-sm-8 form-control" name="Password" id="Password">
                </div>
                <div class="form-group row">
                    <div class="col-sm-5"></div>
                    <button type="submit" class="col-sm-2 btn btn-success" name="btn">ثبت</button>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection
