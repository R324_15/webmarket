@extends('admin.layouts.admin')

@section('title') سایز محصولات @endsection

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    @include('admin.partials.message')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        لیست
        <small>سایز محصولات</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('admin') }}"><i class="fa fa-dashboard"></i> داشبورد</a></li>
        <li class="active"><a href="{{ route('sizes') }}">سایز محصولات</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <a href="{{ route('sizes_create') }}">
                <button type="button" class="btn btn-block btn-success btn-flat">افزودن سایز جدید</button>
              </a>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>شماره</th>
                    <th>محصول</th>
                    <th>عنوان</th>
                    <th>ویرایش</th>
                    <th>حذف</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($sizes as $size)
                    <tr>
                      <td>{{$size->id}}</td>
                      <td><a href="{{ route('product', $size->product->code) }}">{{$size->product->title}}</a></td>
                      <td>{{$size->title}}</td>
                      <td><a href="{{ route('sizes_edit', $size->id) }}" class="btn btn-primary fa fa-edit"></a></td>
                      <td><a href="{{ route('sizes_delete', $size->id) }}" class="btn btn-danger fa fa-remove"></a></td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@section('scripts')

<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
  })
</script>

@endsection

@endsection
