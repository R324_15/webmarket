@extends('admin.layouts.admin')

@section('title') ویرایش محصول @endsection

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    @include('admin.partials.message')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        ویرایش
        <small>محصول شماره {{$product->id}}</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('admin') }}"><i class="fa fa-dashboard"></i> داشبورد</a></li>
        <li><a href="{{ route('products') }}">محصولات</a></li>
        <li class="active"><a href="{{ route('products_edit', $product->id) }}">ویرایش</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

          <!-- Default box -->
          <div class="box">
        <div class="box-body">
          <form class="form-horizontal" action="{{ route('products_update', $product->id) }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group row">
              <div class="col-sm-1"></div>
              <label for="Title" class="col-sm-2 col-form-label">عنوان</label>
              <input type="text" class="col-sm-8 form-control" value="{{$product->title}}" name="Title" id="Title">
            </div>
            <div class="form-group row">
              <div class="col-sm-1"></div>
              <label for="Code" class="col-sm-2 col-form-label">کد</label>
              <input type="number" class="col-sm-8 form-control" value="{{$product->code}}" name="Code" id="Code">
            </div>
            <hr>
            <div class="form-group row">
              <div class="col-sm-1"></div>
              <label for="Type" class="col-sm-2 col-form-label">نوع محصول</label>
                <select class="col-sm-6 form-control" size="1" name="Type" id="Type">
                  @php
                    foreach ( $product_types as $product_type ) {
                      if ($product_type->id == $product->productType_id) {
                        echo "<option value='" . $product->productType_id . "' selected>" . $product_type->id . "- " . $product_type->title . "</option>";
                      } else {
                        echo "<option value='" . $product_type->id . "'>" . $product_type->id . "- " . $product_type->title . "</option>";
                      }
                    }
                  @endphp
                </select>
              <div class="col-sm-1"></div>
              <a class="col-sm-1 btn btn-primary" href="{{ route('types_create') }}">افزودن</a>
            </div>
            <hr>
            <div class="form-group row">
              <div class="col-sm-1"></div>
              <label for="Brand" class="col-sm-2 col-form-label">برند</label>
                <select class="col-sm-6 form-control" size="1" name="Brand" id="Brand">
                  @php
                    foreach ( $product_brands as $product_brand ) {
                      if ($product_brand->id == $product->productBrand_id) {
                        echo "<option value='" . $product->productBrand_id . "' selected>" . $product_brand->id . "- " . $product_brand->title . "</option>";
                      } else {
                        echo "<option value='" . $product_brand->id . "'>" . $product_brand->id . "- " . $product_brand->title . "</option>";
                      }
                    }
                  @endphp
                </select>
              <div class="col-sm-1"></div>
              <a class="col-sm-1 btn btn-primary" href="{{ route('brands_create') }}">افزودن</a>
            </div>
            <hr>
            <div class="form-group row">
              <div class="col-sm-1"></div>
              <label for="Size" class="col-sm-2 col-form-label">دسته</label>
              <a class="col-sm-3 btn btn-info" href="{{ route('categories') }}" style="color: black">لیست</a>
              <div class="col-sm-2"></div>
              <a class="col-sm-3 btn btn-primary" href="{{ route('product_categories_create', $product->id) }}">افزودن</a>
            </div>
            <hr>
            <div class="form-group row">
              <div class="col-sm-1"></div>
              <label for="Color" class="col-sm-2 col-form-label">رنگ</label>
              <a class="col-sm-3 btn btn-info" href="{{ route('colors') }}" style="color: black">لیست</a>
              <div class="col-sm-2"></div>
              <a class="col-sm-3 btn btn-primary" href="{{ route('product_colors_create', $product->id) }}">افزودن</a>
            </div>
            <hr>
            <div class="form-group row">
              <div class="col-sm-1"></div>
              <label for="Size" class="col-sm-2 col-form-label">اندازه</label>
              <a class="col-sm-3 btn btn-info" href="{{ route('sizes') }}" style="color: black">لیست</a>
              <div class="col-sm-2"></div>
              <a class="col-sm-3 btn btn-primary" href="{{ route('product_sizes_create', $product->id) }}">افزودن</a>
            </div>
            <hr>
            <div class="form-group row">
              <div class="col-sm-1"></div>
              <label for="Quantity" class="col-sm-2 col-form-label">تعداد</label>
              <input type="number" class="col-sm-8 form-control" value="{{$product->quantity}}" name="Quantity" id="Quantity">
            </div>
            <div class="form-group row">
              <div class="col-sm-1"></div>
              <label for="Price" class="col-sm-2 col-form-label">قیمت</label>
              <input type="number" class="col-sm-8 form-control" value="{{$product->price}}" name="Price" id="Price">
            </div>
            <div class="form-group row">
              <div class="col-sm-1"></div>
              <label for="Description" class="col-sm-2 col-form-label">توضیحات</label>
              <textarea class="col-sm-8 form-control" name="Description" id="Description">{{$product->description}}</textarea>
            </div>
            <div class="form-group row">
                <div class="col-sm-1"></div>
                <label for="Image" class="col-sm-2 col-form-label">تصویر</label>
                <input class="col-sm-8" type="file" name="image[]" multiple="multiple">
            </div>
            <div class="form-group row">
                <div class="col-sm-2"></div>
                <div class="col-sm-9">
                  @foreach ($unserialize_images as $unserialize_image)
                    <a href="{{ route('product_images', $product->id) }}">
                      <img src="/{{$unserialize_image}}" alt="{{$unserialize_image}}" width="200px" height="150px" style="border: 1px solid black">
                    </a>
                  @endforeach
                </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-5"></div>
              <button type="submit" class="col-sm-2 btn btn-success" name="btn">ثبت</button>
            </div>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection
