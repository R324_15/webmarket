@extends('admin.layouts.admin')

@section('title') افزودن محصول @endsection

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                افزودن
                <small>محصول</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('admin') }}"><i class="fa fa-dashboard"></i> داشبورد</a></li>
                <li><a href="{{ route('products') }}">محصولات</a></li>
                <li class="active"><a href="{{ route('products_create') }}">افزودن</a></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">
                <div class="box-body">
                    <form action="{{ route('products_store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label for="Title" class="col-sm-2 col-form-label">عنوان</label>
                            <input type="text" class="col-sm-8 form-control" name="Title" id="Title">
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label for="Code" class="col-sm-2 col-form-label">کد</label>
                            <input type="number" class="col-sm-8 form-control" name="Code" id="Code">
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label for="Type" class="col-sm-2 col-form-label">نوع محصول</label>
                            <select class="col-sm-6 form-control" size="1" name="Type" id="Type">
                                @foreach($product_types as $product_type)
                                    <option value="{{$product_type->id}}">{{$product_type->title}}</option>
                                @endforeach
                            </select>
                            <div class="col-sm-1"></div>
                            <a class="col-sm-1 btn btn-primary" href="{{ route('types_create') }}">افزودن</a>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label for="Brand" class="col-sm-2 col-form-label">برند</label>
                            <select class="col-sm-6 form-control" size="1" name="Brand" id="Brand">
                                @foreach($product_brands as $product_brand)
                                    <option value="{{$product_brand->id}}">{{$product_brand->title}}</option>
                                @endforeach
                            </select>
                            <div class="col-sm-1"></div>
                            <a class="col-sm-1 btn btn-primary" href="{{ route('brands_create') }}">افزودن</a>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label for="Quantity" class="col-sm-2 col-form-label">تعداد</label>
                            <input type="number" class="col-sm-8 form-control" name="Quantity" id="Quantity">
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label for="Price" class="col-sm-2 col-form-label">قیمت</label>
                            <input type="number" class="col-sm-8 form-control" name="Price" id="Price">
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label for="Description" class="col-sm-2 col-form-label">توضیحات</label>
                            <textarea class="col-sm-8 form-control" name="Description" id="Description"></textarea>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label for="Image" class="col-sm-2 col-form-label">تصویر</label>
                            <input class="col-sm-8" type="file" name="image[]" multiple="multiple">
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-5"></div>
                            <button type="submit" class="col-sm-2 btn btn-success" name="btn"><b>ثبت</b></button>
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection
