@extends('admin.layouts.admin')

@section('title') محصولات @endsection

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        @include('admin.partials.message')

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$product->title}}
                <small>محصول شماره {{$product->id}}</small>
                <a href="{{ Auth::check() ? route('products_edit', $product->id) : NULL }}" class="btn btn-primary">ویرایش</a>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('admin') }}"><i class="fa fa-dashboard"></i> داشبورد</a></li>
                <li><a href="{{ route('products') }}">محصولات <small>(محصول: {{$product->title}})</small></a></li>
                <li class="active"><a href="{{ route('product_images', $product->id) }}">تصاویر</a></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="box">
                <div class="box-body table-responsive" style="background-color: gainsboro">
                    <table id="example1" class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <td>تصویر</td>
                                @foreach ($unserialize_images as $unserialize_image)
                                    <td>
                                        <img src="/{{$unserialize_image}}" alt="{{$unserialize_image}}">
                                    </td>
                                @endforeach
                            </tr>
                            <tr>
                                <td>نام</td>
                                @foreach ($unserialize_images as $unserialize_image)
                                    <td>
                                        {{$unserialize_image}}
                                    </td>
                                @endforeach
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@section('scripts')

    <script language = "text/Javascript"> 
      cleared[0] = cleared[1] = cleared[2] = 0; //set a cleared flag for each field
      function clearField(t){                   //declaring the array outside of the
      if(! cleared[t.id]){                      // function makes it static and global
          cleared[t.id] = 1;  // you could use true and false, but that's more typing
          t.value='';         // with more chance of typos
          t.style.color='#fff';
          }
      }
    </script>

@endsection

@endsection
