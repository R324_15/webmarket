@extends('admin.layouts.admin')

@section('title') محصولات @endsection

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        @include('admin.partials.message')

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                لیست
                <small>محصولات</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('admin') }}"><i class="fa fa-dashboard"></i> داشبورد</a></li>
                <li class="active"><a href="{{ route('products') }}">محصولات</a></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <a href="{{ route('products_create') }}">
                                <button type="button" class="btn btn-block btn-success btn-flat">افزودن محصول جدید</button>
                            </a>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>شماره</th>
                                        <th>تصویر</th>
                                        <th>عنوان</th>
                                        <th>نوع محصول</th>
                                        <th>برند</th>
                                        <th>رنگ</th>
                                        <th>دسته</th>
                                        <th>کد</th>
                                        <th>تعداد</th>
                                        <th>سایز</th>
                                        <th>قیمت</th>
                                        <th>توضیحات</th>
                                        <th>ویرایش</th>
                                        <th>حذف</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($products as $product)
                                        <tr>
                                            <td>{{$product->id}}</td>
                                            <td>
                                                @php
                                                    $unserialize_images = unserialize($product->image);
                                                @endphp
                                                <a href="{{ route('product_images', $product->id) }}">
                                                    <img src="/{{$unserialize_images[0]}}" alt="img" width="70px" height="70px">
                                                </a>
                                            </td>
                                            <td><a href="{{ route('product', $product->code) }}">{{$product->title}}</a></td>
                                            <td>{{$product->product_type->title}}</td>
                                            <td>{{$product->product_brand->title}}</td>
                                            <td>
                                                @if (empty($product->product_colors[0]))
                                                    <a href="{{ route('product_colors_create', $product->id) }}" class="btn btn-info" style="color: black"> افزودن </a>
                                                @else
                                                    @foreach ($product->product_colors as $product_color)
                                                        {{$product_color->name}}</br>
                                                    @endforeach
                                                @endif
                                            </td>
                                            <td>
                                                @if (empty($product->product_categories[0]))
                                                    <a href="{{ route('product_categories_create', $product->id) }}" class="btn btn-info" style="color: black"> افزودن </a>
                                                @else
                                                    @foreach ($product->product_categories as $product_category)
                                                        {{$product_category->title}}</br>
                                                    @endforeach
                                                @endif
                                            </td>
                                            <td>{{$product->code}}</td>
                                            <td>{{$product->quantity}}</td>
                                            <td>
                                                @if (empty($product->product_sizes[0]))
                                                    <a href="{{ route('product_sizes_create', $product->id) }}" class="btn btn-info" style="color: black"> افزودن </a>
                                                @else
                                                    @foreach ($product->product_sizes as $product_size)
                                                        {{$product_size->title}}</br>
                                                    @endforeach
                                                @endif
                                            </td>
                                            <td>{{number_format($product->price)}} تومان</td>
                                            <td>{{$product->description}}</td>
                                            <td><a href="{{ route('products_edit', $product->id) }}" class="btn btn-primary fa fa-edit"></a></td>
                                            <td><a href="{{ route('products_delete', $product->id) }}" class="btn btn-danger fa fa-remove"></a></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@section('scripts')

<!-- page script -->
<script>
    $(function () {
        $('#example1').DataTable()
    })
</script>

@endsection

@endsection
