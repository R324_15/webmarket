@extends('admin.layouts.admin')

@section('title') افزودن نوع محصول @endsection

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                افزودن
                <small>نوع محصول</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('admin') }}"><i class="fa fa-dashboard"></i> داشبورد</a></li>
                <li><a href="{{ route('types') }}">نوع محصولات</a></li>
                <li class="active"><a href="{{ route('types_create') }}">افزودن</a></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">
                <div class="box-body">
                    <form action="{{ route('types_store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label for="Title" class="col-sm-2 col-form-label">عنوان</label>
                            <input type="text" class="col-sm-8 form-control" name="Title" id="Title">
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-5"></div>
                            <button type="submit" class="col-sm-2 btn btn-success" name="btn"><b>ثبت</b></button>
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection
