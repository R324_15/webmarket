@extends('admin.layouts.admin')

@section('title') ویرایش نوع محصول @endsection

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    @include('admin.partials.message')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        ویرایش
        <small>نوع محصول</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('admin') }}"><i class="fa fa-dashboard"></i> داشبورد</a></li>
        <li><a href="{{ route('types') }}">نوع محصولات</a></li>
        <li class="active"><a href="{{ route('types_edit', $type->id) }}">ویرایش</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

          <!-- Default box -->
          <div class="box">
        <div class="box-body">
            <form class="form-horizontal" action="{{ route('types_update', $type->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label for="Title" class="col-sm-2 col-form-label">عنوان</label>
                    <input type="text" class="col-sm-8 form-control" value="{{$type->title}}" name="Title" id="Title">
                </div>
                <div class="form-group row">
                    <div class="col-sm-5"></div>
                    <button type="submit" class="col-sm-2 btn btn-success" name="btn">ثبت</button>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection
