@extends('admin.layouts.admin')

@section('title') افزودن دسته بندی @endsection

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                افزودن
                <small>دسته بندی</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('admin') }}"><i class="fa fa-dashboard"></i> داشبورد</a></li>
                <li><a href="{{ route('categories') }}">دسته بندی محصولات</a></li>
                <li class="active"><a href="{{ route('categories_create') }}">افزودن</a></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">
                <div class="box-body">
                    <form action="{{ route('categories_store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label for="Product" class="col-sm-2 col-form-label">محصول</label>
                            <select class="col-sm-8 form-control" size="1" name="Product" id="Product">
                                @foreach($products as $product)
                                    <option value="{{$product->id}}">{{$product->id}}- {{$product->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label for="Title" class="col-sm-2 col-form-label">عنوان</label>
                            <input type="text" class="col-sm-8 form-control" name="Title" id="Title">
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-5"></div>
                            <button type="submit" class="col-sm-2 btn btn-success" name="btn"><b>ثبت</b></button>
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection