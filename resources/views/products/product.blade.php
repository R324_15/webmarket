@extends('layouts.app')

@section('title') {{$product->title}} @endsection

@section('content')

<!--  ==========  -->
<!--  = Breadcrumbs =  -->
<!--  ==========  -->
<div class="darker-stripe">
    <div class="container">
        <div class="row">
            <div class="span12">
                <ul class="breadcrumb">
                    <li>
                        <a href="{{ route('index') }}">وب‌مارکت</a>
                    </li>
                    <li><span class="icon-chevron-right"></span></li>
                    <li>
                        <a href="{{ route('shop') }}">فروشگاه</a>
                    </li>
                    <li><span class="icon-chevron-right"></span></li>
                    <li>
                        <a href="#">محصول {{$product->product_type->title}}</a>
                    </li>
                    <li><span class="icon-chevron-right"></span></li>
                    <li>
                        <a href="{{ route('product', $product->code) }}">{{$product->title}}</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<!--  ==========  -->
<!--  = Main container =  -->
<!--  ==========  -->
<div class="container">
    <div class="push-up top-equal blocks-spacer">

        <!--  ==========  -->
        <!--  = Product =  -->
        <!--  ==========  -->
        <div class="row blocks-spacer">

            <!--  ==========  -->
            <!--  = Preview Images =  -->
            <!--  ==========  -->
            @php
                $unserialize_images = unserialize($product->image);
            @endphp
            <div class="span5">
                <div class="product-preview">
                    <div class="picture">
                        <img src="/{{$unserialize_images[0]}}" alt="" width="940" height="940" id="mainPreviewImg" />
                    </div>
                    <div class="thumbs clearfix">
                        @for ($i = 0; $i < count($unserialize_images); $i++)
                            <div class="thumb {{ $i == 0 ? 'active' : NULL}}">
                                <a href="#mainPreviewImg"><img src="/{{$unserialize_images[$i]}}" alt="{{$unserialize_images[$i]}}" width="940" height="940" /></a>
                            </div>
                        @endfor
                    </div>
                </div>
            </div>

            <!--  ==========  -->
            <!--  = Title and short desc =  -->
            <!--  ==========  -->
            <div class="span7">
                <div class="product-title">
                    <h1 class="name">{{$product->title}} <small class="light">محصول {{$product->product_type->title}}</small></h1>
                    </br>
                    <div class="meta">
                        <span class="tag">قیمت: {{number_format($product->price)}} تومان</span>
                        </br>
                        </br>
                        <span class="stock">
                            @if ($product->quantity == 0 || !isset($product_colors) || !isset($product_sizes))
                                <span class="btn btn-danger">اتمام موجودی</span>
                            @else
                                <span class="btn btn-success">موجود</span>
                            @endif
                            <a href="{{ route('contact-us') }}" class="btn btn-warning">تماس بگیرید</a>
                        </span>
                    </div>
                </div>
                <div class="product-description">
                    <p>{{$product->description}}</p>
                    <hr />

                    <!--  ==========  -->
                    <!--  = Add to cart form =  -->
                    <!--  ==========  -->
                    <form action="{{ route('cart_store') }}" class="form form-inline clearfix" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="Id" id="Id" value="{{$product->id}}" />
                        <label for="Quantity">تعداد:</label>
                        <div class="numbered">
                            <input type="text" name="Quantity" value="{{$product->quantity == 0 ? '0' : '1'}}" class="tiny-size" {{$product->quantity == 0 ? 'disabled="disabled"' : NULL}} />
                            @if ($product->quantity != 0)
                                <span class="clickable add-one icon-plus-sign-alt"></span>
                                <span class="clickable remove-one icon-minus-sign-alt"></span>
                            @endif
                        </div>
                        &nbsp;
                        <label for="Color">رنگ:</label>
                        <select name="Color" class="span2">
                            @php
                                if ($product->quantity == 0 || !isset($product_colors) || count($product_colors) == 0) {
                                    echo "<option> --- </option>";
                                } else {
                                    foreach ( $product_colors as $product_color )
                                        echo "<option value='" . $product_color->name . "'>" . $product_color->name . "</option>";
                                }
                            @endphp
                        </select>
                        &nbsp;
                        <label for="Size">اندازه:</label>
                        <select name="Size" class="span2">
                            @php
                                if ($product->quantity == 0 || !isset($product_sizes) || count($product_sizes) == 0) {
                                    echo "<option> --- </option>";
                                } else {
                                    foreach ( $product_sizes as $product_size )
                                        echo "<option value='" . $product_size->title . "'>" . $product_size->title . "</option>";
                                }
                            @endphp
                        </select>
                        @if (empty($user))
                            <button type="submit" class="btn btn-danger pull-right" href="#registerModal" {{$product->quantity == 0 ? 'disabled="disabled"' : NULL}} role="button" data-toggle="modal"><i class="icon-shopping-cart"></i> اضافه به سبد خرید</button>
                        @else
                            <button type="submit" class="btn btn-danger pull-right" {{$product->quantity == 0 || count($product_colors) == 0 || count($product_sizes) == 0 || count($products_related) == 0 ? 'disabled="disabled"' : NULL}}><i class="icon-shopping-cart"></i> اضافه به سبد خرید</button>
                        @endif
                    </form>
                    <hr />

                    <!--  ==========  -->
                    <!--  = Share buttons =  -->
                    <!--  ==========  -->
                    <div class="share-item">
                        <div class="pull-right social-networks">
                            <!-- AddThis Button BEGIN -->
                            <div class="addthis_toolbox addthis_default_style ">
                            <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
                            <a class="addthis_button_tweet"></a>
                            <a class="addthis_button_pinterest_pinit"></a>
                            <a class="addthis_counter addthis_pill_style"></a>
                            </div>
                            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-517459541beb3977"></script>
                            <!-- AddThis Button END -->
                        </div>
                        با دوستان خود به اشتراک بگذارید :
                    </div>
                </div>
            </div>
        </div>

        <!--  ==========  -->
        <!--  = Tabs with more info =  -->
        <!--  ==========  -->
        <div class="row">
            <div class="span12">
                <ul id="myTab" class="nav nav-tabs">
                    <li class="active">
                        <a href="#tab-1" data-toggle="tab">جزئیات</a>
                    </li>
                    <li>
                        <a href="#tab-2" data-toggle="tab">جدول اندازه</a>
                    </li>
                    <li>
                        <a href="#tab-3" data-toggle="tab">نظرات</a>
                    </li>
                    <li>
                        <a href="#tab-4" data-toggle="tab">جزئیات ارسال</a>
                    </li>
                    <li></li>
                </ul>
                <div class="tab-content">
                    <div class="fade in tab-pane active" id="tab-1">
                        <h3>توضیحات محصول</h3>
                        <p>{{$product->description}}</p>
                        <h3>برند</h3>
                        <p>{{$product->product_brand->title}}</p>
                    </div>
                    <div class="fade tab-pane" id="tab-2">
                        <img src="/images/size/size.jpg" alt="" width="540" height="374" />
                    </div>
                    <div class="fade tab-pane" id="tab-3">
                        <p>
                            لورم ایپسوم متنی است که ساختگی برای طراحی و چاپ آن مورد است. صنعت چاپ زمانی لازم بود شرایطی شما باید فکر ثبت نام و طراحی، لازمه خروج می باشد. در ضمن قاعده همفکری ها جوابگوی سئوالات زیاد شاید باشد، آنچنان که لازم بود طراحی گرافیکی خوب بود. کتابهای زیادی شرایط سخت ، دشوار و کمی در سالهای دور لازم است. هدف از این نسخه فرهنگ پس از آن و دستاوردهای خوب شاید باشد. حروفچینی لازم در شرایط فعلی لازمه تکنولوژی بود که گذشته، حال و آینده را شامل گردد. سی و پنج درصد از طراحان در قرن پانزدهم میبایست پرینتر در ستون و سطر حروف لازم است، بلکه شناخت این ابزار گاه اساسا بدون هدف بود و سئوالهای زیادی در گذشته بوجود می آید، تنها لازمه آن بود.
                        </p>
                    </div>
                    <div class="fade tab-pane" id="tab-4">
                        <p>
                            لورم ایپسوم متنی است که ساختگی برای طراحی و چاپ آن مورد است. صنعت چاپ زمانی لازم بود شرایطی شما باید فکر ثبت نام و طراحی، لازمه خروج می باشد. در ضمن قاعده همفکری ها جوابگوی سئوالات زیاد شاید باشد، آنچنان که لازم بود طراحی گرافیکی خوب بود. کتابهای زیادی شرایط سخت ، دشوار و کمی در سالهای دور لازم است. هدف از این نسخه فرهنگ پس از آن و دستاوردهای خوب شاید باشد. حروفچینی لازم در شرایط فعلی لازمه تکنولوژی بود که گذشته، حال و آینده را شامل گردد. سی و پنج درصد از طراحان در قرن پانزدهم میبایست پرینتر در ستون و سطر حروف لازم است، بلکه شناخت این ابزار گاه اساسا بدون هدف بود و سئوالهای زیادی در گذشته بوجود می آید، تنها لازمه آن بود...
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /container -->

@if (isset($products_related) && count($products_related) > 1)
    <!--  ==========  -->
    <!--  = Related Products =  -->
    <!--  ==========  -->
    <div class="boxed-area no-bottom">
        <div class="container">

            <!--  ==========  -->
            <!--  = Title =  -->
            <!--  ==========  -->
            <div class="row">
                <div class="span12">
                    <div class="main-titles lined">
                        <h2 class="title"><span class="light">محصولات</span> مرتبط</h2>
                    </div>
                </div>
            </div>

            <!--  ==========  -->
            <!--  = Related products =  -->
            <!--  ==========  -->
            <div class="row popup-products">

                <!--  ==========  -->
                <!--  = Products =  -->
                <!--  ==========  -->

                @foreach($products_related as $product)
                    @if ($product->product->code == $code)
                    @else
                        <div class="span3">
                            @include('partials.product-related-widget')
                        </div>
                    @endif
                @endforeach

                <!-- /product -->

            </div>
        </div>
    </div>
    <!-- end of master-wrapper -->
@endif

@endsection
