@extends('layouts.app')

@section('title') فروشگاه @endsection

@section('content')

<!--  ==========  -->
<!--  = Breadcrumbs =  -->
<!--  ==========  -->
<div class="darker-stripe">
    <div class="container">
        <div class="row">
            <div class="span12">
                <ul class="breadcrumb">
                    <li>
                        <a href="{{ route('index') }}">وب‌مارکت</a>
                    </li>
                    <li><span class="icon-chevron-right"></span></li>
                    <li>
                        <a href="{{ route('shop') }}">فروشگاه</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="push-up blocks-spacer">
        <div class="row">

            <!--  ==========  -->
            <!--  = Sidebar =  -->
            <!--  ==========  -->
            <aside class="span3 left-sidebar" id="tourStep1">
                <div class="sidebar-item sidebar-filters" id="sidebarFilters">

                    <!--  ==========  -->
                    <!--  = Sidebar Title =  -->
                    <!--  ==========  -->
                    <div class="underlined">
                        <h3><span class="light">بر اساس فیلتر</span> خرید کنید</h3>
                    </div>

                    <!--  ==========  -->
                    <!--  = Categories =  -->
                    <!--  ==========  -->
                    <div class="accordion-group" id="tourStep2">
                        <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" href="#filterOne">دسته بندی ها <b class="caret"></b></a>
                        </div>
                        <div id="filterOne" class="accordion-body collapse in">
                            <div class="accordion-inner">
                                @foreach ($product_categories as $product_category)
                                    <a href="#" data-target="{{$product_category->title}}" data-type="category" class="selectable detailed"><i class="box"></i> {{$product_category->title}}</a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!-- /categories -->

                    <!--  ==========  -->
                    <!--  = Prices slider =  -->
                    <!--  ==========  -->
                    <!-- <div class="accordion-group">
                        <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" href="#filterPrices">قیمت <b class="caret"></b></a>
                        </div>
                        <div id="filterPrices" class="accordion-body in collapse">
                            <div class="accordion-inner with-slider">
                                <div class="jqueryui-slider-container">
                                    <div id="pricesRange"></div>
                                </div>
                                <input type="text" data-initial="432" class="max-val pull-right" disabled />
                                <input type="text" data-initial="99" class="min-val" disabled />
                            </div>
                        </div>
                    </div> -->
                        <!-- /prices slider -->

                    <!--  ==========  -->
                    <!--  = Color filter =  -->
                    <!--  ==========  -->
                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <a class="accordion-toggle collapsed" data-toggle="collapse" href="#filterTwo">رنگ <b class="caret"></b></a>
                        </div>
                        <div id="filterTwo" class="accordion-body collapse">
                            <div class="accordion-inner">
                                <a href="#" data-target="سفید" data-type="color" class="selectable detailed"><i class="box"></i> سفید</a>
                                <a href="#" data-target="سیاه" data-type="color" class="selectable detailed"><i class="box"></i> سیاه</a>
                                <a href="#" data-target="خاکستری" data-type="color" class="selectable detailed"><i class="box"></i> خاکستری</a>
                                <a href="#" data-target="قهوه ای" data-type="color" class="selectable detailed"><i class="box"></i> قهوه ای</a>
                                <a href="#" data-target="قرمز" data-type="color" class="selectable detailed"><i class="box"></i> قرمز</a>
                                <a href="#" data-target="صورتی" data-type="color" class="selectable detailed"><i class="box"></i> صورتی</a>
                                <a href="#" data-target="بنفش" data-type="color" class="selectable detailed"><i class="box"></i> بنفش</a>
                                <a href="#" data-target="آبی" data-type="color" class="selectable detailed"><i class="box"></i> آبی</a>
                                <a href="#" data-target="سبز" data-type="color" class="selectable detailed"><i class="box"></i> سبز</a>
                                <a href="#" data-target="زرد" data-type="color" class="selectable detailed"><i class="box"></i> زرد</a>
                                <a href="#" data-target="نارنجی" data-type="color" class="selectable detailed"><i class="box"></i> نارنجی</a>
                            </div>
                        </div>
                    </div>
                    <!-- /color filter -->

                    <!--  ==========  -->
                    <!--  = Brand filter =  -->
                    <!--  ==========  -->
                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <a class="accordion-toggle collapsed" data-toggle="collapse" href="#filterThree">برند <b class="caret"></b></a>
                        </div>
                        <div id="filterThree" class="accordion-body collapse">
                            <div class="accordion-inner">
                                @foreach ($product_brands as $product_brand)
                                    <a href="#" data-target="{{$product_brand->title}}" data-type="brand" class="selectable detailed"><i class="box"></i> {{$product_brand->title}}</a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!-- /brand filter -->
                    <a href="#" class="remove-filter" id="removeFilters"><span class="icon-ban-circle"></span> حذف همه فیلتر ها</a>
                </div>
            </aside>
            <!-- /sidebar -->

            <!--  ==========  -->
            <!--  = Main content =  -->
            <!--  ==========  -->
            <section class="span9">

                <!--  ==========  -->
                <!--  = Title =  -->
                <!--  ==========  -->
                <div class="underlined push-down-20">
                    <div class="row">
                        <div class="span5">
                            <h3><span class="light">همه</span> محصولات</h3>
                        </div>
                        <div class="span4">
                            <div class="form-inline sorting-by" id="tourStep4">
                                <label for="isotopeSorting" class="black-clr">چینش :</label>
                                <select id="isotopeSorting" class="span3">
                                    <option value='{"sortBy":"price", "sortAscending":"true"}'>بر اساس قیمت (کم به زیاد) &uarr;</option>
                                    <option value='{"sortBy":"price", "sortAscending":"false"}'>بر اساس قیمت (زیاد به کم) &darr;</option>
                                    <option value='{"sortBy":"name", "sortAscending":"true"}'>بر اساس نام (صعودی) &uarr;</option>
                                    <option value='{"sortBy":"name", "sortAscending":"false"}'>بر اساس نام (نزولی) &darr;</option>
                                    <option value='{"sortBy":"popularity", "sortAscending":"true"}'>بر اساس محبوبیت (کم به زیاد) &uarr;</option>
                                    <option value='{"sortBy":"popularity", "sortAscending":"false"}'>بر اساس محبوبیت (زیاد به کم) &darr;</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /title -->

                <!--  ==========  -->
                <!--  = Products =  -->
                <!--  ==========  -->
                <div class="row popup-products">
                    <div id="isotopeContainer" class="isotope-container">

                        <!--  ==========  -->
                        <!--  = Single Product =  -->
                        <!--  ==========  -->

                        @foreach($products as $product)
                            <div class="span3" data-category="@foreach($product->product_categories as $category){{$category->title}}|@endforeach" data-price="{{$product->price}}" data-popularity="{{$product->product_type->id == 3 ? 1 : 0}}" data-color="@foreach($product->product_colors as $color){{$color->name}}|@endforeach" data-brand="{{$product->product_brand->title}}">
                                @include('partials.product-widget')
                            </div>
                        @endforeach

                        <!-- /single product -->
                    </div>
                </div>
                <hr />
            </section>
            <!-- /main content -->
        </div>
    </div>
</div>
<!-- /container -->
<!-- end of master-wrapper -->

@endsection
