@extends('layouts.app')

@section('title') شیوه‌ی پرداخت - سبد خرید @endsection

@section('content')

<div class="container">
    <div class="row">
        <br />

        <!--  ==========  -->
        <!--  = Main content =  -->
        <!--  ==========  -->
        <section class="span12">
            <div class="checkout-container">
                <div class="row">
                    <div class="span10 offset1">

                        <!--  ==========  -->
                        <!--  = Header =  -->
                        <!--  ==========  -->
                        <header style="padding-top: 20px;">
                            <div class="row">
                                <div class="span7">
                                    <div class="center-align">
                                        <h1><span class="light">انتخاب</span> شيوه پرداخت</h1>
                                    </div>
                                </div>
                                <div class="span3">
                                    <div class="right-align">
                                        <img src="{{ asset('images/buttons/security.jpg') }}" alt="Security Sign" width="92" height="65" />
                                    </div>
                                </div>
                            </div>
                        </header>

                        <!--  ==========  -->
                        <!--  = Steps =  -->
                        <!--  ==========  -->
                        <div class="checkout-steps">
                            <div class="clearfix">
                                <div class="step done">
                                    <div class="step-badge"><i class="icon-ok"></i></div>
                                    <a href="{{ route('cart_step1') }}">سبد خريد</a>
                                </div>
                                <div class="step done">
                                    <div class="step-badge"><i class="icon-ok"></i></div>
                                    <a href="{{ route('cart_step2') }}">آدرس ارسال</a>
                                </div>
                                <div class="step done">
                                    <div class="step-badge"><i class="icon-ok"></i></div>
                                    <a href="{{ route('cart_step3') }}">تاييد و پرداخت</a>
                                </div>
                                <div class="step active">
                                    <div class="step-badge">4</div>
                                    شيوه پرداخت
                                </div>
                            </div>
                        </div>
                        <!-- /steps -->

                        <!--  ==========  -->
                        <!--  = Payment =  -->
                        <!--  ==========  -->
                        <div class="shifted-left-45 clearfix">
                            <h3 class="no-margin"><span class="light">کارت</span> اعتباري</h3>
                            <p class="push-down-30"></p>
                            <form action="#" method="post" class="form-horizontal form-checkout">
                                <div class="control-group">
                                    <label class="control-label" for="firstName">نام<span class="red-clr bold"> *</span></label>
                                    <div class="controls">
                                        <input type="text" id="firstName" class="span4" required>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="lastName">نام خانوادگي<span class="red-clr bold"> *</span></label>
                                    <div class="controls">
                                        <input type="text" id="lastName" class="span4" required>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="cardNum">شماره کارت اعتباري<span class="red-clr bold"> *</span></label>
                                    <div class="controls">
                                        <input type="text" id="cardNum" class="span1 card-num-input center-align" maxlength="4">
                                        <input type="text" class="span1 card-num-input center-align" maxlength="4">
                                        <input type="text" class="span1 card-num-input center-align" maxlength="4">
                                        <input type="text" class="span1 card-num-input center-align add-tooltip" maxlength="4">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="cvv2">CVV2<span class="red-clr bold"> *</span></label>
                                    <div class="controls">
                                        <input id="cvv2" type="text" class="span1 center-align add-tooltip" maxlength="3" required>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="exp">تاريخ انقضا<span class="red-clr bold"> *</span></label>
                                    <div class="controls">
                                        <select id="exp" class="input-small month-push-right">
                                            <option value="-1">ماه</option>
                                            <option value="1">01</option>
                                            <option value="2">02</option>
                                            <option value="3">03</option>
                                            <option value="4">04</option>
                                            <option value="5">05</option>
                                            <option value="6">06</option>
                                            <option value="7">07</option>
                                            <option value="8">08</option>
                                            <option value="9">09</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                        </select>
                                        <select id="exp" class="input-small">
                                            <option value="-1">سال</option>
                                            <option value="2013">2013</option>
                                            <option value="2014">2014</option>
                                            <option value="2015">2015</option>
                                            <option value="2016">2016</option>
                                            <option value="2017">2017</option>
                                            <option value="2018">2018</option>
                                            <option value="2019">2019</option>
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <hr />
                        <p class="right-align">
                            <a href="{{ route('index') }}" class="btn btn-primary higher bold">تاييد</a>
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <!-- /main content -->
    </div>
</div>
<!-- /container -->
<!-- end of master-wrapper -->

    @endsection
