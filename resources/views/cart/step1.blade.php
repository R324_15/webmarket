@extends('layouts.app')

@section('title') بازبینی - سبد خرید @endsection

@section('content')

<div class="container">
	<div class="row">
		<br />

		<!--  ==========  -->
		<!--  = Main content =  -->
		<!--  ==========  -->
		<section class="span12">
			<div class="checkout-container">
				<div class="row">
					<div class="span10 offset1">

						<!--  ==========  -->
						<!--  = Header =  -->
						<!--  ==========  -->
						<header style="padding-top: 20px;">
							<div class="row">
								<div class="span7">
									<div class="center-align">
										<h1><span class="light">بازبینی</span> سبد خرید</h1>
									</div>
								</div>
								<div class="span3">
									<div class="right-align">
										<img src="{{ asset('images/buttons/security.jpg') }}" alt="Security Sign" width="92" height="65" />
									</div>
								</div>
							</div>
						</header>

						<!--  ==========  -->
						<!--  = Steps =  -->
						<!--  ==========  -->
						<div class="checkout-steps">
							<div class="clearfix">
								<div class="step active">
									<div class="step-badge">1</div>
									سبد خرید
								</div>
								<div class="step">
									<div class="step-badge">2</div>
									آدرس ارسال
								</div>
								<div class="step">
									<div class="step-badge">3</div>
									تایید و پرداخت
								</div>
								<div class="step">
									<div class="step-badge">4</div>
									شيوه پرداخت
								</div>
							</div>
						</div>
						<!-- /steps -->

						<!--  ==========  -->
						<!--  = Selected Items =  -->
						<!--  ==========  -->
						<table class="table table-items">
							<thead>
								<tr>
									<th colspan="2">آیتم</th>
									<th colspan="1">رنگ</th>
									<th colspan="1">سایز</th>
									<th><div class="align-center">تعداد</div></th>
									<th><div class="align-right">قیمت</div></th>
								</tr>
							</thead>
							<tbody>
								@php
									$sum_price = 0;
									$i = 0;
									foreach ($cart_products as $product) {
										echo '<tr>';
											echo '<td class="image">';
												$unserialize_images = unserialize($product->image);
												echo '<a href="' . route('product', $product->code) . '">';
													echo '<img src="/' . $unserialize_images[0] . '" alt="" width="124" height="124" />';
												echo '</a>';
											echo '</td>';
											echo '<td class="desc">';
												echo '<a href="' . route('product', $product->code) . '">' . $product->title . '</a> &nbsp;';
											echo '</td>';
											if ($product->code == $carts[$i]->product->code) {
												echo '<td>' . $carts[$i]->product_color . '</td>';
												echo '<td>' . $carts[$i]->product_size . '</td>';
											}
											echo '<td class="qty">';
												echo $carts[$i]->product_quantity;
											echo '</td>';
											echo '<td class="price">';
												$price = $product->price * $carts[$i]->product_quantity;
												echo number_format($price) . ' تومان';
												echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
												echo '<a href="' . route('cart_delete', $carts[$i]->id) . '" style="color: red">حذف</a>';
											echo '</td>';
										echo '</tr>';
										$sum_price += $price;
										$i++;
									}
								@endphp
								<tr>
									<td colspan="4" rowspan="2">
										<div class="alert alert-info">
											<button data-dismiss="alert" class="close" type="button">×</button>
											هزینه ارسال بر اساس منطقه جغرافیایی محاسبه میشود. <!-- <a href="#">اطلاعات بیشتر</a> -->
										</div>
									</td>
									<td class="stronger">هزینه ارسال :</td>
									<td class="stronger"><div class="align-right">15,000 تومان</div></td>
								</tr>
								<tr>
									<td class="stronger">جمع کل :</td>
									<td class="stronger"><div class="size-16 align-right">{{number_format($sum_price += 15000)}} تومان</div></td>
								</tr>
							</tbody>
						</table>
						<p class="right-align">
							در مرحله بعدی شما آدرس ارسال را انتخاب خواهید کرد. &nbsp; &nbsp;
							<a href="{{ route('cart_step2') }}" class="btn btn-primary higher bold">ادامه</a>
						</p>
					</div>
				</div>
			</div>
		</section>
		<!-- /main content -->
	</div>
</div>
<!-- /container -->
<!-- end of master-wrapper -->

	@endsection
