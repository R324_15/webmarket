@extends('layouts.app')

@section('title') آدرس محل - سبد خرید @endsection

@section('content')

<div class="container">
    <div class="row">
        <br />

        <!--  ==========  -->
        <!--  = Main content =  -->
        <!--  ==========  -->
        <section class="span12">
            <div class="checkout-container">
                <div class="row">
                    <div class="span10 offset1">

                        <!--  ==========  -->
                        <!--  = Header =  -->
                        <!--  ==========  -->
                        <header style="padding-top: 20px;">
                            <div class="row">
                                <div class="span7">
                                    <div class="center-align">
                                        <h1><span class="light">آدرس ارسال</span></h1>
                                    </div>
                                </div>
                                <div class="span3">
                                    <div class="right-align">
                                        <img src="{{ asset('images/buttons/security.jpg') }}" alt="Security Sign" width="92" height="65" />
                                    </div>
                                </div>
                            </div>
                        </header>

                        <!--  ==========  -->
                        <!--  = Steps =  -->
                        <!--  ==========  -->
                        <div class="checkout-steps">
                            <div class="clearfix">
                                <div class="step done">
                                    <div class="step-badge"><i class="icon-ok"></i></div>
                                    <a href="{{ route('cart_step1') }}">سبد خرید</a>
                                </div>
                                <div class="step active">
                                    <div class="step-badge">2</div>
                                    آدرس ارسال
                                </div>
                                <div class="step">
                                    <div class="step-badge">3</div>
                                    تایید و پرداخت
                                </div>
                                <div class="step">
                                    <div class="step-badge">4</div>
                                    شيوه پرداخت
                                </div>
                            </div>
                        </div>
                        <!-- /steps -->

                        <!--  ==========  -->
                        <!--  = Shipping addr form =  -->
                        <!--  ==========  -->
                        <form action="#" method="post" class="form-horizontal form-checkout">
                            <div class="control-group">
                                <label class="control-label" for="Nationalcode">کد ملی<span class="red-clr bold"> *</span></label>
                                <div class="controls">
                                    <input type="text" name="Nationalcode" id="Nationalcode" value="{{$user->national_code}}" class="span4" required>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="Name">نام<span class="red-clr bold"> *</span></label>
                                <div class="controls">
                                    <input type="text" name="Name" id="Name" value="{{$user->name}}" class="span4" required>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="Phone">شماره تلفن<span class="red-clr bold"> *</span></label>
                                <div class="controls">
                                    <input type="tel" name="Phone" id="Phone" value="{{$user->phone}}" class="span4" required>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="Province">استان<span class="red-clr bold"> *</span></label>
                                <div class="controls">
                                    <input type="text" name="Province" id="Province" value="{{$user->province}}" class="span4" >
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="City">شهر<span class="red-clr bold"> *</span></label>
                                <div class="controls">
                                    <input type="text" name="City" id="City" value="{{$user->city}}" class="span4" >
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="Address">آدرس<span class="red-clr bold"> *</span></label>
                                <div class="controls">
                                    <input type="text" name="Address" id="Address" value="{{$user->address}}" class="span4" required>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="Postcode">کد پستی<span class="red-clr bold"> *</span></label>
                                <div class="controls">
                                    <input type="text" name="Postcode" id="Postcode" value="{{$user->post_code}}" class="span4" >
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="Email">ایمیل</label>
                                <div class="controls">
                                    <input type="email" name="Email" id="Email" value="{{$user->email}}" class="span4">
                                </div>
                            </div>
                        </form>
                        <hr />
                        <p class="right-align">
                            در مرحله بعدي شما قادر هستيد سفارشتان را بازبيني کرده و آن را تاييد کنيد &nbsp; &nbsp;
                            <a href="{{ route('cart_step3') }}" class="btn btn-primary higher bold">ادامه</a>
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <!-- /main content -->
    </div>
</div>
<!-- /container -->
<!-- end of master-wrapper -->

    @endsection
