<!--  = Login =  -->
<div id="loginModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="loginModalLabel"><span class="light">ورود</span> در وب‌مارکت</h3>
    </div>
    <div class="modal-body">
        <form action="{{ route('login') }}" method="post">
            @csrf
            <div class="control-group">
                <div class="controls">
                    <input type="number" class="input-block-level" id="Phone" name="Phone" placeholder="شماره تلفن">
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <input type="password" class="input-block-level" id="Password" name="Password" placeholder="رمز عبور">
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <label class="checkbox">
                        <input type="checkbox">
                        مرا به خاطر بسپار
                    </label>
                </div>
            </div>
            <button type="submit" class="btn btn-primary input-block-level bold higher">
                ورود
            </button>
        </form>
            <a type="submit" href="#registerModal" class="btn btn-danger input-block-level bold higher" role="button" data-dismiss="modal" data-toggle="modal">
                ثبت نام
            </a>
            </br>
        <p class="center-align push-down-0">
            <a href="#" data-dismiss="modal">رمز عبورتان را فراموش کرده اید؟</a>
        </p>
    </div>
</div>
