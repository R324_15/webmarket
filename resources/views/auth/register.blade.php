    <!--  = Register =  -->
    <div id="registerModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="registerModalLabel"><span class="light">ثبت نام</span> در وب‌مارکت</h3>
        </div>
        <div class="modal-body">
            <form action="{{ route('register') }}" method="post">
                @csrf
                <div class="control-group">
                    <div class="controls">
                        <input type="text" class="input-block-level" id="Name" name="Name" required placeholder="نام کاربری">
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <input type="email" class="input-block-level" id="Email" name="Email" required placeholder="ایمیل">
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <input type="number" class="input-block-level" id="Phone" name="Phone" required placeholder="شماره تلفن">
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <input type="password" class="input-block-level" id="Password" name="Password" required placeholder="رمز عبور">
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <input type="password" class="input-block-level" id="Password-confirm" name="Password_confirmation" required placeholder="تایید رمز عبور">
                    </div>
                </div>
                <button type="submit" class="btn btn-danger input-block-level bold higher">
                    ثبت نام
                </button>
            </form>
            <p class="center-align push-down-0">
                <a data-toggle="modal" role="button" href="#loginModal" data-dismiss="modal">قبلا ثبت نام کرده اید؟</a>
            </p>
        </div>
    </div>
</div>
