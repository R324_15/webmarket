<!DOCTYPE html>

<!--[if lt IE 8]>      <html class="no-js lt-ie10 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie10 lt-ie9"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>@yield('title') | وب‌مارکت</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ProteusThemes">

    <!--  =========  -->
    <!--  = Style =  -->
    <!--  =========  -->

    <!--  Google Fonts  -->
    <link href='http://fonts.googleapis.com/css?family=Pacifico|Open+Sans:400,700,400italic,700italic&amp;subset=latin,latin-ext,greek' rel='stylesheet' type='text/css'>

    <!--  Google Fonts  -->
    <link href='http://fonts.googleapis.com/css?family=Pacifico|Open+Sans:400,700,400italic,700italic&amp;subset=latin,latin-ext,greek' rel='stylesheet' type='text/css'>

    <!-- Twitter Bootstrap -->
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet" type="text/css"/>

    <!-- Slider Revolution -->
    <link href="{{ asset('js/rs-plugin/css/settings.css') }}" rel="stylesheet" type="text/css"/>

    <!-- jQuery UI -->
    <link href="{{ asset('js/jquery-ui-1.10.3/css/smoothness/jquery-ui-1.10.3.custom.min.css') }}" rel="stylesheet" type="text/css"/>

    <!-- PrettyPhoto -->
    <link href="{{ asset('js/prettyphoto/css/prettyPhoto.css') }}" rel="stylesheet" type="text/css"/>

    <!-- main styles -->
    <link href="{{ asset('css/main.css') }}" rel="stylesheet" type="text/css"/>

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('images/apple-touch/144.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('images/apple-touch/114.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('images/apple-touch/72.png') }}">
    <link rel="apple-touch-icon-precomposed" href="{{ asset('images/apple-touch/57.png') }}">
    <link rel="shortcut icon" href="{{ asset('images/apple-touch/57.png') }}">
</head>

<body class="">

    <div class="master-wrapper">

        <!--  ==========  -->
        <!--  = Header =  -->
        <!--  ==========  -->

        @include('partials.header')

        <!-- /header -->

        <!--  ======================  -->
        <!--  = Main Menu / navbar =  -->
        <!--  ======================  -->

        @include('partials.menu')

        <!-- /main menu -->

    @yield('content')

    @include('partials.footer')

    <!--  =================  -->
    <!--  = Modal Windows =  -->
    <!--  =================  -->

    @include('auth.login')

    @include('auth.register')

    <!--  FB  -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=126780447403102";
        fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <!--  jQuery - CDN with local fallback  -->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript">
        if (typeof jQuery == 'undefined') {
            document.write('<script src="{{ asset('js/jquery.min.js') }}"><\/script>');
        }
    </script>

    <!--  ===========  -->
    <!--  = Scripts =  -->
    <!--  ===========  -->
    <script src="{{ asset('js/underscore/underscore-min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/rs-plugin/pluginsources/jquery.themepunch.plugins.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/rs-plugin/js/jquery.themepunch.revolution.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/jquery.carouFredSel-6.2.1-packed.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/jquery-ui-1.10.3/js/jquery-ui-1.10.3.custom.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/jquery-ui-1.10.3/touch-fix.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/isotope/jquery.isotope.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/bootstrap-tour/build/js/bootstrap-tour.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/prettyphoto/js/jquery.prettyPhoto.js') }}" type="text/javascript"></script>
    <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
    <script src="{{ asset('js/goMap/js/jquery.gomap-1.3.2.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/custom.js') }}" type="text/javascript"></script>

</body>

</html>
