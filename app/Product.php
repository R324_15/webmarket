<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded=[];

    public function product_type()
    {
        return $this->hasOne('App\ProductType', 'id', 'productType_id');
    }

    public function product_brand()
    {
        return $this->hasOne('App\ProductBrand', 'id', 'productBrand_id');
    }

    public function product_categories()
    {
        return $this->hasMany('App\ProductCategory');
    }

    public function product_colors()
    {
        return $this->hasMany('App\ProductColor');
    }

    public function product_sizes()
    {
        return $this->hasMany('App\ProductSize');
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'carts');
    }
}
