<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductBrand extends Model
{
    protected $guarded=[];

    public function product()
    {
        return $this->hasOne('App\Product', 'productBrand_id', 'id');
    }
}
