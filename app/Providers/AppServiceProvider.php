<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use App\User;
use App\Cart;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(['layouts.app', 'index', 'products.product', 'cart.step1', 'cart.step3'], function ($view)
        {
            if (isset(auth::user()->id)) {
                $user = User::findOrFail(auth::user()->id);
                $cart_products = $user->products;
                $carts = Cart::where('user_id', auth::user()->id)->get();
                $view->with('cart_products', $cart_products)->with('carts', $carts)->with('user', $user);
            }
        });
    }
}
