<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table='provinces';
    protected $guarded=[];

    public function cities()
    {
        return $this->hasMany(City::class,'province_id');
    }
}
