<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductType extends Model
{
    protected $guarded=[];

    public function product()
    {
        return $this->hasOne('App\Product', 'productType_id', 'id');
    }
}
