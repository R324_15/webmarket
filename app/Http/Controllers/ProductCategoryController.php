<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductCategory;
use App\Product;

class ProductCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = ProductCategory::all();
        return view('admin.products.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Product::all();
        return view('admin.products.categories.create', compact('products'));
    }

    public function product_categories_create($id)
    {
        $products = Product::all();
        return view('admin.products.categories.product_categories_create', compact('products', 'id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'Product' => 'required',
            'Title' => 'required',
        ]);

        $registered = ProductCategory::create([
            'product_id'=>$request->Product,
            'title'=>$request->Title,
        ]);

        if ( $registered ) {
            return redirect()->route('categories', $_REQUEST['btn'])->with('message', 'با موفقیت ایجاد شد');
        } else {
                echo '
                <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>هشدار! </strong>ثبت گروه بندی ناموفق بود... لطفا دوباره تلاش کنید.
                </div>
                ';
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = ProductCategory::findOrFail($id);
        return view('admin.products.categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = ProductCategory::findOrFail($id);

        request()->validate([
            'Title' => 'required',
        ]);

        $category->update([
            'product_id'=>$category->product_id,
            'title'=>$request->Title,
		]);

        return redirect()->route('categories_edit', $category->id)->with('message', 'با موفقیت بروز شد');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = ProductCategory::findOrFail($id);
        $category->delete();
        return redirect()->route('categories', $id)->with('message', 'با موفقیت حذف شد');
    }
}
