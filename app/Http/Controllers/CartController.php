<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Cart;
use App\User;
use App\Product;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth::user()->user_type == 0) {
            $carts = Cart::all();
            return view('admin.carts.index', compact('carts'));
        } else {
            return view('errors.404');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth::user()->user_type == 0) {
            $users = User::all();
            $products = Product::all();
            return view('admin.carts.create', compact('users', 'products'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $carts = Cart::where('user_id', $request->User)->where('product_id', $request->Product)->get();

        request()->validate([
            'User' => 'required',
            'Product' => 'required',
            'Color' => 'required',
            'Size' => 'required',
            'Quantity' => 'required',
        ]);

        if (count($carts) > 0) {
            foreach ($carts as $cart) {
                if ($cart->user_id == $request->User && $cart->product_id == $request->Product) {
                    $quantity = $cart->product_quantity + $request->Quantity;
                    $registered = $cart->update([
                        'product_quantity'=>$quantity,
                    ]);
                }
            }
        } else {
            $registered = Cart::create([
                'user_id'=>$request->User,
                'product_id'=>$request->Product,
                'product_color'=>$request->Color,
                'product_size'=>$request->Size,
                'product_quantity'=>$request->Quantity,
            ]);
        }

        if ( $registered ) {
            return redirect()->route('carts', $_REQUEST['btn'])->with('message', 'با موفقیت ایجاد شد');
        } else {
                echo '
                <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>هشدار! </strong>ثبت سبد خرید ناموفق بود... لطفا دوباره تلاش کنید.
                </div>
                ';
        }
    }

    public function cart_store(Request $request)
    {
        $carts = Cart::where('user_id', auth::user()->id)->where('product_id', $request->Id)->get();

        request()->validate([
            'Quantity' => 'required',
            'Color' => 'required',
            'Size' => 'required',
        ]);

        if (count($carts) > 0) {
            $i = count($carts) - 1;
            $done = false;
            foreach ($carts as $cart) {
                if ($cart->product_color == $request->Color && $cart->product_size == $request->Size) {
                    $quantity = $cart->product_quantity + $request->Quantity;
                    $done = $cart->update([
                        'product_quantity'=>$quantity,
                    ]);
                }
                if ($i == 0 && $done == false) {
                    Cart::create([
                        'user_id'=>auth::user()->id,
                        'product_id'=>$request->Id,
                        'product_quantity'=>$request->Quantity,
                        'product_color'=>$request->Color,
                        'product_size'=>$request->Size,
                    ]);
                }
                $i--;
            }
        } else {
            Cart::create([
                'user_id'=>auth::user()->id,
                'product_id'=>$request->Id,
                'product_quantity'=>$request->Quantity,
                'product_color'=>$request->Color,
                'product_size'=>$request->Size,
            ]);
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (auth::user()->user_type == 0) {
            $cart = Cart::findOrFail($id);
            $users = User::all();
            $products = Product::all();
            return view('admin.carts.edit', compact('cart', 'users', 'products'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cart = Cart::findOrFail($id);

        request()->validate([
            'User' => 'required',
            'Product' => 'required',
            'Color' => 'required',
            'Size' => 'required',
            'Quantity' => 'required',
        ]);

        $cart->update([
            'user_id'=>$request->User,
            'product_id'=>$request->Product,
            'product_color'=>$request->Color,
            'product_size'=>$request->Size,
            'product_quantity'=>$request->Quantity,
		]);

        return redirect()->route('carts_edit', $cart->id)->with('message', 'با موفقیت بروز شد');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cart::findOrFail($id)->delete();
        return redirect()->route('carts', $id)->with('message', 'با موفقیت حذف شد');
    }

    public function cart_delete($id)
    {
        $cart = Cart::findOrFail($id);
        $cart->delete();
        return redirect()->back();
    }
}
