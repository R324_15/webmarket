<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductSize;
use App\Product;

class ProductSizeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sizes = ProductSize::all();
        return view('admin.products.sizes.index', compact('sizes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Product::all();
        return view('admin.products.sizes.create', compact('products'));
    }

    public function product_sizes_create($id)
    {
        $products = Product::all();
        return view('admin.products.sizes.product_sizes_create', compact('products', 'id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'Product' => 'required',
            'Title' => 'required',
        ]);

        $registered = ProductSize::create([
            'product_id'=>$request->Product,
            'title'=>$request->Title,
        ]);

        if ( $registered ) {
            return redirect()->route('sizes', $_REQUEST['btn'])->with('message', 'با موفقیت ایجاد شد');
        } else {
                echo '
                <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>هشدار! </strong>ثبت گروه بندی ناموفق بود... لطفا دوباره تلاش کنید.
                </div>
                ';
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductSize  $productSize
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductSize  $productSize
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $size = ProductSize::findOrFail($id);
        return view('admin.products.sizes.edit', compact('size'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductSize  $productSize
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $size = ProductSize::findOrFail($id);

        request()->validate([
            'Title' => 'required',
        ]);

        $size->update([
            'product_id'=>$size->product_id,
            'title'=>$request->Title,
		]);

        return redirect()->route('sizes_edit', $size->id)->with('message', 'با موفقیت بروز شد');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductSize  $productSize
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $size = ProductSize::findOrFail($id);
        $size->delete();
        return redirect()->route('sizes', $id)->with('message', 'با موفقیت حذف شد');
    }
}
