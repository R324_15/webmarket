<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator,Redirect,Response,File;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Product;
use App\ProductType;
use App\ProductBrand;
use App\ProductColor;
use App\ProductSize;
use App\ProductCategory;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        $product_categories = ProductCategory::all();
        $product_colors = ProductColor::all();
        $product_brands = ProductBrand::all();
        return view('products.shop', compact('products', 'product_categories', 'product_colors', 'product_brands'));
    }

    public function index_admin()
    {
        $products = Product::all();
        return view('admin.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product_types = ProductType::all();
        $product_brands = ProductBrand::all();
        return view('admin.products.create', compact('product_types', 'product_brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'Type' => 'required',
            'Brand' => 'required',
            'Code' => 'required',
            'Title' => 'required',
            'Quantity' => 'required',
            'Price' => 'required',
            'Description' => 'required',
            'image' => 'required',
            'image.*' => 'mimes:jpeg,png,jpg,gif,svg'
        ]);

        if($request->hasfile('image')) {
            $i = 1;
            foreach($request->file('image') as $file) {
                $image = $i . time() . '.' . $file->getClientOriginalName();
                $file->move(public_path().'/product_images/', $image);
                $image_array[] = "product_images\\$image";
                $i++;
            }
            $serialize_images = serialize($image_array);
        }

        $registered = Product::create([
            'productType_id'=>$request->Type,
            'productBrand_id'=>$request->Brand,
            'code'=>$request->Code,
            'title'=>$request->Title,
            'quantity'=>$request->Quantity,
            'price'=>$request->Price,
            'description'=>$request->Description,
            'image'=>$serialize_images,
        ]);

        if ( $registered ) {
            return redirect()->route('products', $_REQUEST['btn'])->with('message', 'با موفقیت ایجاد شد');
        } else {
                echo '
                <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>هشدار! </strong>ثبت محصول ناموفق بود... لطفا دوباره تلاش کنید.
                </div>
                ';
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($code)
    {
        $product = Product::whereCode($code)->firstOrFail();
        $product_colors = ProductColor::where('product_id', $product->id)->get();
        $product_sizes = ProductSize::where('product_id', $product->id)->get();
        $product_categories = ProductCategory::where('product_id', $product->id)->get();
        foreach ($product_categories as $product_categorie) {
            $products_related = ProductCategory::where('title', $product_categorie->title)->get();
        }
        if ($product_colors || $product_sizes || $product_categories) {
            return view('products.product', compact('product', 'products_related', 'product_colors', 'product_sizes', 'code'));
        } else
            return view('products.product', compact('product', 'code'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);
        $unserialize_images = unserialize($product->image);
        $product_types = ProductType::all();
        $product_brands = ProductBrand::all();
        return view('admin.products.edit', compact('product', 'product_types', 'product_brands', 'unserialize_images'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::findOrFail($id);

        request()->validate([
            'Type' => 'required',
            'Brand' => 'required',
            'Code' => 'required',
            'Title' => 'required',
            'Quantity' => 'required',
            'Price' => 'required',
            'Description' => 'required',
        ]);

        if($request->hasfile('image')) {
            request()->validate([
                'image' => 'required',
                'image.*' => 'mimes:jpeg,png,jpg,gif,svg'
            ]);

            $i = 1;
            foreach($request->file('image') as $file) {
                $image = $i . time() . '.' . $file->getClientOriginalName();
                $file->move(public_path().'/product_images/', $image);
                $image_array[] = "product_images\\$image";
                $i++;
            }
            $serialize_images = serialize($image_array);
            if ($serialize_images) {
                $unserialize_images = unserialize($product->image);
                $count_images = count($unserialize_images);
                for ($i = 0; $i < $count_images; $i++) {
                    File::delete($unserialize_images[$i]);
                }
            }
        } else {
            $serialize_images = "$product->image";
        }

        $product->update([
            'productType_id'=>$request->Type,
            'productBrand_id'=>$request->Brand,
            'code'=>$request->Code,
            'title'=>$request->Title,
            'quantity'=>$request->Quantity,
            'price'=>$request->Price,
            'description'=>$request->Description,
            'image'=>$serialize_images,
        ]);

        return redirect()->route('products_edit', $product->id)->with('message', 'با موفقیت بروز شد');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);

        foreach ($product->product_colors as $product_color) {
            $color = ProductColor::findOrFail($product_color->id);
            $color->delete();
        }

        foreach ($product->product_sizes as $product_size) {
            $size = ProductSize::findOrFail($product_size->id);
            $size->delete();
        }

        foreach ($product->product_categories as $product_category) {
            $category = ProductCategory::findOrFail($product_category->id);
            $category->delete();
        }

        $product->delete();

        $unserialize_images = unserialize($product->image);
        $count_images = count($unserialize_images);
        for ($i = 0; $i < $count_images; $i++) {
            File::delete($unserialize_images[$i]);
        }

        return redirect()->route('products', $id)->with('message', 'با موفقیت حذف شد');
    }

    //Product Images
    public function product_images($id)
    {
        $product = Product::findOrFail($id);
        $unserialize_images = unserialize($product->image);
        return view('admin.products.images', compact('product', 'unserialize_images'));
    }

    //Steps for Cart
    public function cart_step1()
    {
        if (isset(auth::user()->id) || !empty($cart_products[0])) {
            return view('cart.step1');
        } else {
            return view('errors.404');
        }
    }

    public function cart_step2()
    {
        if (isset(auth::user()->id)) {
            $user = User::findOrFail(auth::user()->id);
            return view('cart.step2', compact('user'));
        } else {
            return view('errors.404');
        }
    }

    public function cart_step3()
    {
        return view('cart.step3');
    }

    public function cart_step4()
    {
        return view('cart.step4');
    }
}
