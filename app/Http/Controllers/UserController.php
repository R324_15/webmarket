<?php

namespace App\Http\Controllers;

use App\City;
use App\Province;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create')->with(['provinces'=>Province::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'Usertype' => 'required',
            'Name' => 'required',
            'Email' => 'required',
            'Phone' => 'required',
            'Password' => 'required',
        ]);

        $registered = User::create([
            'province_id'=>$request->province_id,
            'city_id'=>$request->city_id,
            'user_type'=>$request->Usertype,
            'name'=>$request->Name,
            'email'=>$request->Email,
            'national_code'=>$request->Nationalcode,
            'address'=>$request->Address,
            'post_code'=>$request->Postcode,
            'phone'=>$request->Phone,
            'password'=>Hash::make($request->Password),
        ]);

        if ( $registered ) {
            return redirect()->route('users', $_REQUEST['btn'])->with('message', 'با موفقیت ایجاد شد');
        } else {
                echo '
                <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>هشدار! </strong>ثبت کاربر ناموفق بود... لطفا دوباره تلاش کنید.
                </div>
                ';
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $provinces = Province::all();
        if(!empty($user->province_id)){
            $cities = Province::where('id', $user->province_id)->first()->cities()->get();
        } else {
            $cities = City::all();
        }
        return view('admin.users.edit', compact('user', 'provinces', 'cities'));
    }

    public function customer_edit($id)
    {
        $user = User::findOrFail($id);
        return view('admin.users.customer_edit', compact('user'));
    }

    public function customer_update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        request()->validate([
            'Name' => 'required',
            'Email' => 'required',
            'Phone' => 'required',
        ]);

        if ($request->filled('password')) {
            $Newpassword = Hash::make($request->password);
        } else {
            $Newpassword = $user->password;
        }

        $user->update([
            'province_id'=>$request->province_id,
            'city_id'=>$request->city_id,
            'user_type'=>'1',
            'national_code'=>$request->Nationalcode,
            'name'=>$request->Name,
            'email'=>$request->Email,
            'address'=>$request->Address,
            'post_code'=>$request->Postcode,
            'phone'=>$request->Phone,
            'password'=>$Newpassword,
		]);

        return redirect()->route('users_customer_edit', $user->id)->with('message', 'با موفقیت بروز شد');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        request()->validate([
            'Usertype' => 'required',
            'Name' => 'required',
            'Email' => 'required',
            'Phone' => 'required',
        ]);

        if ($request->filled('password')) {
            $Newpassword = Hash::make($request->password);
        } else {
            $Newpassword = $user->password;
        }

        $user->update([
            'province_id'=>$request->province_id,
            'city_id'=>$request->city_id,
            'user_type'=>$request->Usertype,
            'national_code'=>$request->Nationalcode,
            'name'=>$request->Name,
            'email'=>$request->Email,
            'address'=>$request->Address,
            'post_code'=>$request->Postcode,
            'phone'=>$request->Phone,
            'password'=>$Newpassword,
		]);

        return redirect()->route('users_edit', $user->id)->with('message', 'با موفقیت بروز شد');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return redirect()->route('users', $id)->with('message', 'با موفقیت حذف شد');
    }
}
