<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Product;
use App\User;
use App\Cart;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $count_products = count(Product::get());
        $products = Product::all();
        $products_new = Product::where('productType_id', '1')->orderBy('id', 'desc')->take(8)->get();
        $products_special = Product::where('productType_id', '2')->orderBy('id', 'desc')->take(3)->get();
        $products_favorite = Product::where('productType_id', '3')->orderBy('id', 'desc')->take(3)->get();
        return view('index', compact('products', 'products_new', 'products_special', 'products_favorite', 'count_products'));
    }

    public function dashboard()
    {
        $users = User::where('user_type', 1)->get();
        $carts = Cart::all();
        return view('admin.dashboard', compact('users', 'carts'));
    }

    public function about()
    {
        return view('pages.about-us');
    }

    public function contact()
    {
        $date = date('D');
        return view('pages.contact-us', compact('date'));
    }

    public function error()
    {
        return view('errors.404');
    }
}
