<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductType;

class ProductTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types = ProductType::all();
        return view('admin.products.types.index', compact('types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.products.types.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'Title' => 'required',
        ]);

        $registered = ProductType::create([
            'title'=>$request->Title,
        ]);

        if ( $registered ) {
            return redirect()->route('types', $_REQUEST['btn'])->with('message', 'با موفقیت ایجاد شد');
        } else {
                echo '
                <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>هشدار! </strong>ثبت نوع محصول ناموفق بود... لطفا دوباره تلاش کنید.
                </div>
                ';
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $type = ProductType::findOrFail($id);
        return view('admin.products.types.edit', compact('type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $type = ProductType::findOrFail($id);

        request()->validate([
            'Title' => 'required',
        ]);

        $type->update([
            'title'=>$request->Title,
		]);

        return redirect()->route('types_edit', $type->id)->with('message', 'با موفقیت بروز شد');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $type = ProductType::findOrFail($id);
        $type->delete();
        return redirect()->route('types', $id)->with('message', 'با موفقیت حذف شد');
    }
}
