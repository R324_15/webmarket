<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductColor;
use App\Product;

class ProductColorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $colors = ProductColor::all();
        return view('admin.products.colors.index', compact('colors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Product::all();
        return view('admin.products.colors.create', compact('products'));
    }

    public function product_colors_create($id)
    {
        $products = Product::all();
        return view('admin.products.colors.product_colors_create', compact('products', 'id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'Product' => 'required',
            'Name' => 'required',
        ]);

        $registered = ProductColor::create([
            'product_id'=>$request->Product,
            'name'=>$request->Name,
        ]);

        if ( $registered ) {
            return redirect()->route('colors', $_REQUEST['btn'])->with('message', 'با موفقیت ایجاد شد');
        } else {
                echo '
                <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>هشدار! </strong>ثبت رنگ ناموفق بود... لطفا دوباره تلاش کنید.
                </div>
                ';
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $color = ProductColor::findOrFail($id);
        return view('admin.products.colors.edit', compact('color'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $color = ProductColor::findOrFail($id);

        request()->validate([
            'Name' => 'required',
        ]);

        $color->update([
            'product_id'=>$color->product_id,
            'name'=>$request->Name,
		]);

        return redirect()->route('colors_edit', $color->id)->with('message', 'با موفقیت بروز شد');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $color = ProductColor::findOrFail($id);
        $color->delete();
        return redirect()->route('colors', $id)->with('message', 'با موفقیت حذف شد');
    }
}
