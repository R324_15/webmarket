<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::ADMIN;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm(Request $request)
    {
        return redirect('/');
    }

    public function login(Request $request)
    {
        $input = $request->all();

        $this->validate($request, [
            'Phone' => 'required',
            'Password' => 'required',
        ]);

        if(auth()->attempt(array('phone' => $input['Phone'], 'password' => $input['Password'])))
        {
            return redirect()->route('admin');
        } else {
            return redirect()->route('index')
                ->with('error','ایمیل یا رمز عبور اشتباه می باشد');
        }
    }

    public function logout() {
        Auth::logout();
        return redirect('/');
    }
}
