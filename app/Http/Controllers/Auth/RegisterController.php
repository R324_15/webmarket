<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::ADMIN;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'password' => Hash::make($data['password']),
        ]);
    }

    public function showRegistrationForm(Request $request)
    {
        return redirect('/');
    }

    public function register(Request $request)
    {
        $this->validate($request,[
          	'Name'=>'required',
           	'Email'=>'required',
            'Phone' => 'required',
        	'Password'=>'required|min:6|confirmed',
        ]);
        $user = User::create([
            'user_type'=>1,
            'name'=>$request->Name,
            'email'=>$request->Email,
            'phone'=>$request->Phone,
            'password'=>Hash::make($request->Password),
        ]);

        if (Auth::login($user)){
            return redirect($this->redirectPath())->with(['message'=>'کاربر گرامی ثبت شما با موفقیت انجام شد']);
        } else {
            return redirect()->route('login')->with(['message'=>'کاربر گرامی ثبت نام شما با موفقیت انجام شده ، اکنون میتوانید وارد حساب کاربریتان شوید']);
        }


    }
}
