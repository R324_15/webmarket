<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->unsignedBigInteger('productType_id')->after('id');
            $table->foreign('productType_id')->references('id')->on('product_types');
            $table->unsignedBigInteger('productBrand_id')->after('productType_id');
            $table->foreign('productBrand_id')->references('id')->on('product_brands');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropForeign('products_productType_id_foreign');
            $table->dropForeign('products_productBrand_id_foreign');
        });
    }
}