<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// User Authentication
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

//Admin Pages
Route::group(['prefix'=>'/admin', 'middleware' => ['auth','admin']], function ()
{
    Route::get('','HomeController@dashboard')->name('admin');
    Route::get('customer_edit/{id}', 'UserController@customer_edit')->name('users_customer_edit');
    Route::post('customer_update/{id}', 'UserController@customer_update')->name('users_customer_update');

    Route::group(['prefix'=>'/carts/'], function(){
        Route::get('', 'CartController@index')->name('carts');
        Route::get('create', 'CartController@create')->name('carts_create');
        Route::post('store', 'CartController@store')->name('carts_store');
        Route::get('edit/{id}', 'CartController@edit')->name('carts_edit');
        Route::post('update/{id}', 'CartController@update')->name('carts_update');
        Route::get('delete/{id}', 'CartController@destroy')->name('carts_delete');
    });

    Route::group(['prefix'=>'/users/'], function(){
        Route::get('', 'UserController@index')->name('users');
        Route::get('create', 'UserController@create')->name('users_create');
        Route::post('store', 'UserController@store')->name('users_store');
        Route::get('edit/{id}', 'UserController@edit')->name('users_edit');
        Route::post('update/{id}', 'UserController@update')->name('users_update');
        Route::get('delete/{id}', 'UserController@destroy')->name('users_delete');
    });

    Route::group(['prefix'=>'/products/'], function(){
        Route::get('', 'ProductController@index_admin')->name('products');
        Route::get('create', 'ProductController@create')->name('products_create');
        Route::post('store', 'ProductController@store')->name('products_store');
        Route::get('edit/{id}', 'ProductController@edit')->name('products_edit');
        Route::post('update/{id}', 'ProductController@update')->name('products_update');
        Route::get('delete/{id}', 'ProductController@destroy')->name('products_delete');
        Route::get('images/{id}', 'ProductController@product_images')->name('product_images');

        Route::group(['prefix'=>'/colors/'], function(){
            Route::get('', 'ProductColorController@index')->name('colors');
            Route::get('create', 'ProductColorController@create')->name('colors_create');
            Route::get('product_colors_create/{id}', 'ProductColorController@product_colors_create')->name('product_colors_create');
            Route::post('store', 'ProductColorController@store')->name('colors_store');
            Route::get('edit/{id}', 'ProductColorController@edit')->name('colors_edit');
            Route::post('update/{id}', 'ProductColorController@update')->name('colors_update');
            Route::get('delete/{id}', 'ProductColorController@destroy')->name('colors_delete');
        });

        Route::group(['prefix'=>'/types/'], function(){
            Route::get('', 'ProductTypeController@index')->name('types');
            Route::get('create', 'ProductTypeController@create')->name('types_create');
            Route::post('store', 'ProductTypeController@store')->name('types_store');
            Route::get('edit/{id}', 'ProductTypeController@edit')->name('types_edit');
            Route::post('update/{id}', 'ProductTypeController@update')->name('types_update');
            Route::get('delete/{id}', 'ProductTypeController@destroy')->name('types_delete');
        });

        Route::group(['prefix'=>'/categories/'], function(){
            Route::get('', 'ProductCategoryController@index')->name('categories');
            Route::get('create', 'ProductCategoryController@create')->name('categories_create');
            Route::get('product_categories_create/{id}', 'ProductCategoryController@product_categories_create')->name('product_categories_create');
            Route::post('store', 'ProductCategoryController@store')->name('categories_store');
            Route::get('edit/{id}', 'ProductCategoryController@edit')->name('categories_edit');
            Route::post('update/{id}', 'ProductCategoryController@update')->name('categories_update');
            Route::get('delete/{id}', 'ProductCategoryController@destroy')->name('categories_delete');
        });

        Route::group(['prefix'=>'/sizes/'], function(){
            Route::get('', 'ProductSizeController@index')->name('sizes');
            Route::get('create', 'ProductSizeController@create')->name('sizes_create');
            Route::get('product_sizes_create/{id}', 'ProductSizeController@product_sizes_create')->name('product_sizes_create');
            Route::post('store', 'ProductSizeController@store')->name('sizes_store');
            Route::get('edit/{id}', 'ProductSizeController@edit')->name('sizes_edit');
            Route::post('update/{id}', 'ProductSizeController@update')->name('sizes_update');
            Route::get('delete/{id}', 'ProductSizeController@destroy')->name('sizes_delete');
        });

        Route::group(['prefix'=>'/brands/'], function(){
            Route::get('', 'ProductBrandController@index')->name('brands');
            Route::get('create', 'ProductBrandController@create')->name('brands_create');
            Route::post('store', 'ProductBrandController@store')->name('brands_store');
            Route::get('edit/{id}', 'ProductBrandController@edit')->name('brands_edit');
            Route::post('update/{id}', 'ProductBrandController@update')->name('brands_update');
            Route::get('delete/{id}', 'ProductBrandController@destroy')->name('brands_delete');
        });
    });

});

//Cart
Route::post('/store','CartController@cart_store')->name('cart_store');
Route::get('/delete/{id}','CartController@cart_delete')->name('cart_delete');

//Main-Pages
Route::get('/', "HomeController@index")->name('index');
Route::get('/about-us', "HomeController@about")->name('about-us');
Route::get('/contact-us', "HomeController@contact")->name('contact-us');

//Error
Route::get('/error-404', "HomeController@error")->name('error');

Route::get('/shop', 'ProductController@index')->name('shop');
Route::get('/product/{code}', 'ProductController@show')->name('product');

//Cart
Route::group(['prefix'=>'cart'], function ()
{
    Route::get('/cart_step1', 'ProductController@cart_step1')->name('cart_step1');
    Route::get('/cart_step2', 'ProductController@cart_step2')->name('cart_step2');
    Route::get('/cart_step3', 'ProductController@cart_step3')->name('cart_step3');
    Route::get('/cart_step4', 'ProductController@cart_step4')->name('cart_step4');
});
